# TnetRUI [<img src="man/figures/AE.png" align="right" width=160 height=160 alt=""/>](https://www.inrae.fr/en)

<!-- badges: start -->
[![Lifecycle:Experimental](https://img.shields.io/badge/Lifecycle-Experimental-339999)](<Redirect-URL>)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)
<!-- badges: end -->

R package to prepare and read results from T-NET

This project was carried out for National Research Institute for Agriculture, Food and the Environment (Institut National de Recherche pour l’Agriculture, l’Alimentation et l’Environnement, [INRAE](https://agriculture.gouv.fr/inrae-linstitut-national-de-recherche-pour-lagriculture-lalimentation-et-lenvironnement) in french).


## Installation
For latest development version
``` r
remotes::install_gitlab(repo = 'guillaume.hevin/tnet_rui', host = 'forgemia.inra.fr')
```

Details about installation can be found [here](https://tnet-rui-guillaume-hevin-ff27f1d42adcce7ceedbb780ef0d88b292c862.pages.mia.inra.fr/articles/TnetRUI_1.html)

## Documentation

All documentation about TnetRUI can be found on this [website](https://tnet-rui-guillaume-hevin-ff27f1d42adcce7ceedbb780ef0d88b292c862.pages.mia.inra.fr)


## Licenses
### J2000
J2000 Saône exemple data provided in this package are licensed under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1) and are provided 'AS IS', with no warranties.

### SAFRAN
SAFRAN exemple data provided in this package are provided by "Météo-France" and are royalty-free under [Licence Ouverte d'Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2014/05/Licence_Ouverte.pdf)

####

## Code of Conduct
Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

