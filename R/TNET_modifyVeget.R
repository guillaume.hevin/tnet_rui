

#' @title Modify vegetation density and heights on segments
#' @description
#' Change the vegetation density and height on each segment. The modification can be done on all banks or only on right or left banks.
#' 
#' @param filename Path to the shapefile with all info on segments created by [TNET_createShape()]. It must contain the following columns: 
#' |                                |                                                                                                    |
#' |--------------------------------|----------------------------------------------------------------------------------------------------|
#' | \eqn{gid}{gid}                 | ID of the Topage segment                                                                           |
#' | \eqn{vegPctL}{vegPctL}         | Percentage of the segment left bank length covered by vegetation (number between 0 and 100)        |
#' | \eqn{vegPctR}{vegPctR}         | Percentage of the segment right bank length covered by vegetation (number between 0 and 100)       |
#' | \eqn{vegHL}{vegHL}             | Mean height of the vegetation on the segment left bank (in meter)                                  |
#' | \eqn{vegHR}{vegHR}             | Mean height of the vegetation on the segment right bank (in meter)                                 |
#' 
#' @param density *expression (see details section)*. Set the new vegetation density on left and right banks.
#' @param density.left *expression (see details section)*. Set the new vegetation density on left bank only.
#' @param density.right *expression (see details section)*. Set the new vegetation density on right bank only.
#' @param height *expression (see details section)*. Set the new vegetation height on left and right banks.
#' @param height.left *expression (see details section)*. Set the new vegetation height on left bank only.
#' @param height.right *expression (see details section)*. Set the new vegetation height on right bank only.
#' @param select_ID *string or numerical vector* Expression that select segments on which the modification will be set or a vector of gids to be affected by the vegetation modification.
#' 
#' @details
#' **Height and density selection**
#' `density`, `density.left` and `density.right` expression must be one of the following (exemple on `density` argument, but work also on `density.left` and `density.right` arguments)
#' - **`density = 30`**  : will set all vegetation density to `30%`.
#' - **`density = +30`** : will add `30%` of the segment length to the vegetation (`60%` will become `90%`).
#' - **`density = ++30`**: will add `30%` of the vegetation density to the vegetation (`60%` will become `78%`, because `0.3*60 = 18`)
#' 
#' For `height`, `height.left` and `height.right` expression must be one of the following (exemple on `height` argument, but work also on `height.left` and `height.right` arguments)
#' - **`height = 5`**   : will set all vegetation height to `5m`.
#' - **`height = +5`**  : will add `5m` to all vegetation height (`15m` will become `20m`).
#' - **`height = ++5`** : will add `5%` of the vegetation height to the height (`15m` will become `15.75m`, because `0.05*15 = 0.75`)
#' 
#' Signs that can be use in the expression are **`+`** or **`++`** to add vegetation, and **`-`** or **`--`** to remove vegetation.\cr
#' `density` cannot be more than 100% and less than 0 and `height` cannont be less than 0.
#' 
#' **Select_ID selection**
#' It's possible to affect Height and density changes only on selected segments. It's possible to select affected elements with an expression containing columns contain 
#' in `filename` shapefile (for exemple `"OSTRAHL <= 2"`). You can also give a vector of selected gid you want to change.
#' 
#' @return path to the shapefile created with modified vegetation and a unique filename based on realised calculation
#' @export
#' 
#' @seealso [TNET_initializeSim()] [TNET_createShape()]
#' @examples
#' #reading shapefile 
#' filename = path/to/shapefile.shp
#' 
#' st_read(filename)
#'#>          gid     vegPctL    vegPctR
#'#>1      540646  75.0560512  73.362396
#'#>2      918148  53.5128923  17.492715
#'#>3      748051  38.8304411  60.178841
#'#>4      686534  67.0719322  78.928194
#'#>5       23797  17.6317680  14.724709
#'
#'
#'
#'
#' ### Put all vegetation density to 40% ###
#' 
#' new_filename = TNET_modifyVeget(filename,
#'                                 density = 40)
#' 
#' st_read(new_filename) 
#'#>          gid    vegPctL    vegPctR
#'#>1      540646         40         40
#'#>2      918148         40         40
#'#>3      748051         40         40
#'#>4      686534         40         40
#'#>5       23797         40         40
#'
#'
#'
#'
#' ### add 30% of segment length to left bank if it's below 50% ###
#' 
#' new_filename = TNET_modifyVeget(filename,
#'                                 density.left = +30,
#'                                 select_ID = "vegPctL < 50")
#' 
#' st_read(new_filename) 
#'#>          gid    vegPctL    vegPctR
#'#>1      540646   75.05605   73.36240
#'#>2      918148   53.51289   17.49271
#'#>3      748051   68.83044   60.17884
#'#>4      686534   67.07193   78.92819
#'#>5       23797   47.63177   14.72471
#'
#'
#'
#'
#' ### remove 30% of segment length to the left bank       ###
#' ### and add 20% of the vegetation density to right bank ###
#' ### only on 540646 and 748051 segments                  ###
#' 
#' new_filename = TNET_modifyVeget(filename,
#'                                 density.left = -30,
#'                                 density.right = ++20,
#'                                 select_ID = c(540646,748051))
#' 
#' st_read(new_filename) 
#'#>          gid    vegPctL    vegPctR
#'#>1      540646  45.056051   88.03488
#'#>2      918148  53.512892   17.49271
#'#>3      748051   8.830441   72.21461
#'#>4      686534  67.071932   78.92819
#'#>5       23797  17.631768   14.72470
#'
#'
#'
#'
#' ### if you use the function TNET_initializeSim()  you can save  ###
#' ### the new shapefile path in the "shapeName" argument of the   ###
#' ### infoSim list                                                ###
#' 
#' infoSim = TNET_initializeSim(...) #see TNET_initializeSim() documentation
#' 
#' #Create the shapefile for TNET
#' TNET_createShape(infoSim$TOPAGE_shape, infoSim$shapeName)
#' 
#' #Modify the vegetation
#' infoSim$shapeName = TNET_modifyVeget(infoSim$shapeName,
#'                                      density.left = 50,
#'                                      density.right = 0)
#' 
#' st_read(infoSim$TOPAGE_shape) 
#'#>          gid    vegPctL    vegPctR
#'#>1      540646         50          0
#'#>2      918148         50          0
#'#>3      748051         50          0
#'#>4      686534         50          0
#'#>5       23797         50          0


TNET_modifyVeget <- function(filename, density = NULL, height = NULL, density.left = NULL, density.right = NULL, height.left = NULL, height.right = NULL, select_ID = NULL ) {
  
  #Get value of arguments----
  if (is.null(density) & is.null(density.left) & is.null(density.right) & 
      is.null(height) & is.null(height.left) & is.null(height.right)) {
    stop('At least one of "density","density.left", "density.right", "height","height.left", "height.right" argument must be set')
  }
  
  ##Get density modification----
  modify_density = FALSE
  d_case = 0
  if (!is.null(density) | !is.null(density.left) | !is.null(density.right)){
    #If we want to modify vegetation dentity
    modify_density = TRUE
    
    if (!is.null(density) & is.null(density.left) & is.null(density.right)) {
      #If density is set, it will be use for density.left and density.right
      d_case = 1
      if (is.character(density)){
        density.left  <- density
        density.right <- density
      }else{
        density.left  <- deparse(substitute(density))
        density.right <- deparse(substitute(density))
      }
      
    } else if (!is.null(density.right) & is.null(density.left)) {
      #If only density.right is set, density.left will not be changed (--> +0)
      d_case = 2
      density.left <- "+0"
      if (!is.character(density.right)){
        density.right <- deparse(substitute(density.right))
      }
    } else if (!is.null(density.left) & is.null(density.right)) {
      #If only density.left is set, density.right will not be changed (--> +0)
      d_case = 3
      density.right <- "+0"
      if (!is.character(density.left)){
        density.left <- deparse(substitute(density.left))
      }
    }else if (!is.null(density.left) & !is.null(density.right)) {
      #If density.left and density.right are set, there are used
      d_case = 4
      if (!is.character(density.right)){
        density.right <- deparse(substitute(density.right))
      }
      if (!is.character(density.left)){
        density.left <- deparse(substitute(density.left))
      }
    }
  }
  
  ##Get height modification----
  modify_height = FALSE
  h_case = 0
  if (!is.null(height) | !is.null(height.left) | !is.null(height.right)){
    modify_height = TRUE
    
    if (!is.null(height) & is.null(height.left) & is.null(height.right)) {
      #If height is set, it will be use for height.left and height.right
      h_case = 1
      if (is.character(height)){
        height.left  <- height
        height.right <- height
      }else{
        height.left  <- deparse(substitute(height))
        height.right <- deparse(substitute(height))
      }
      
    } else if (!is.null(height.right) & is.null(height.left)) {
      #If only height.right is set, height.left will not be changed (--> +0)
      h_case = 2
      height.left <- "+0"
      if (!is.character(height.right)){
        height.right <- deparse(substitute(height.right))
      }
    } else if (!is.null(height.left) & is.null(height.right)) {
      #If only height.left is set, height.right will not be changed (--> +0)
      h_case = 3
      height.right <- "+0"
      if (!is.character(height.left)){
        height.left <- deparse(substitute(height.left))
      }
    }else if (!is.null(height.left) & !is.null(height.right)) {
      #If height.left and height.right are set, there are used
      h_case = 4
      if (!is.character(height.right)){
        height.right <- deparse(substitute(height.right))
      }
      if (!is.character(height.left)){
        height.left <- deparse(substitute(height.left))
      }
    }
  }
  
  
  #Recognize action to do----
  recognize <- function(text) {
    #Operation to be recognize
    operations = '+-'
    
    #Getting the first character
    action = substring(text,1,1)
    
    
    if (grepl(action, operations)){
      #If the first character is + or -
      fix_value = FALSE
      
      #We read the 2nd character
      actionb = substring(text,2,2)
      
      if (action == actionb){
        #If we are in a case "++" or "--", the value start at the third character
        as_not_percent <- FALSE
        value_start <- 3
      }else{
        #If we are in a case "+" or "-", the value start at the 2nd character
        as_not_percent <- TRUE
        value_start <- 2
      }
      
      #getting the value as numeric
      value_ask <- as.numeric(substring(text,value_start))
      
      #Changing final value depending on the action 
      if (action == '+' & as_not_percent){
        value <- value_ask
        act_txt = paste0('+',value_ask)
      }else if (action == '-' & as_not_percent){
        value <- - value_ask
        act_txt = paste0('-',value_ask)
      }else if (action == '+') {
        value <- 1 + value_ask/100
        act_txt = paste0('++',value_ask)
      }else if (action == '-') {
        value <- 1 - value_ask/100
        act_txt = paste0('--',value_ask)
      }
    }else {
      #If we just have a number (fix value)
      fix_value <- TRUE
      value <- as.numeric(text)
      as_not_percent <- NULL
      act_txt = paste0(value)
    }
    
    result <- list(fix_value = fix_value,
                  value = value,
                  as_not_percent = as_not_percent,
                  act_txt = text)
    
    return(result)
  }
  
  
  #Open shapefile----
  
  shapefile_all <- st_read(filename, quiet = TRUE)
  
  #Select ask lines
  if (!is.null(select_ID)) {
    if (is.character(select_ID)) {
      shapefile = subset(shapefile_all, eval(parse(text = select_ID)))
      cat(sprintf(' %.0f segments are selected with the expression %s\n',nrow(shapefile),select_ID))
      select_asExpression = TRUE
    } else if (is.numeric(select_ID)) {
      shapefile = shapefile_all[shapefile_all$gid %in% select_ID,]
      cat(sprintf(' %.0f segments are selected based on select_ID vector argument\n',nrow(shapefile)))
      select_asExpression = FALSE
    } else {
      stop('select_ID argument must be an expression as character or a vector of gid to select')
    }
    
  }else{
    shapefile = shapefile_all
  }
  shapefile_notmodif <- shapefile_all[!shapefile_all$gid %in% shapefile$gid,]
  
  
  
  #Modify vegetation density----
  if (modify_density){
    action_DR = recognize(density.right)
    action_DL = recognize(density.left)
    

    ##Calculations on left bank----
    if (action_DL$fix_value) {
      shapefile$vegPctL <- action_DL$value
    }else {
      if (action_DL$as_not_percent) {
        shapefile$vegPctL <- shapefile$vegPctL + action_DL$value
      } else {
        shapefile$vegPctL <- shapefile$vegPctL * action_DL$value
      }
    }
    
    #put value >100% at 100%
    shapefile$vegPctL[shapefile$vegPctL>100] <- 100
    #put value <0% at 0%
    shapefile$vegPctL[shapefile$vegPctL<0] <- 0
    
    
    #Calculations on right bank----
    if (action_DR$fix_value) {
      shapefile$vegPctR <- action_DR$value
    }else {
      if (action_DR$as_not_percent) {
        shapefile$vegPctR <- shapefile$vegPctR + action_DR$value
      } else {
        shapefile$vegPctR <- shapefile$vegPctR * action_DR$value
      }
    }
    
    #put value >100% at 100%
    shapefile$vegPctR[shapefile$vegPctR>100] <- 100
    #put value <0% at 0%
    shapefile$vegPctR[shapefile$vegPctR<0] <- 0
    
  }

  #Modify vegetation height----
  if (modify_height){
    action_HR = recognize(height.right)
    action_HL = recognize(height.left)
    
    ##Calculations on left bank----
    if (action_HL$fix_value) {
      shapefile$vegHL <- action_HL$value
    }else {
      if (action_HL$as_not_percent) {
        shapefile$vegHL <- shapefile$vegHL + action_HL$value
      } else {
        shapefile$vegHL <- shapefile$vegHL * action_HL$value
      }
    }
    
    #put value <0 at 0
    shapefile$vegHL[shapefile$vegHL<0] <- 0
    
    #Calculations on right bank----
    if (action_HR$fix_value) {
      shapefile$vegHR <- action_HR$value
    }else {
      if (action_HR$as_not_percent) {
        shapefile$vegHR <- shapefile$vegHR + action_HR$value
      } else {
        shapefile$vegHR <- shapefile$vegHR * action_HR$value
      }
    }
    
    #put value <0 at 0
    shapefile$vegHR[shapefile$vegHR<0] <- 0
  }
  
  #Add not modified segments
  shapefile_save <- rbind(shapefile,shapefile_notmodif)
  shapefile_save <- shapefile_save[order(shapefile_save$gid),]
  #save shapefile----
  
  namefile <- basename(filename)
  
  ##create new name----
  name = "_VEG"
  
  ##Adding density modif in name----
  if (d_case == 1) {
    name = paste0(name,'_Density_',action_DR$act_txt)
  }else if (d_case == 2) {
    name = paste0(name,'_DensityR_',action_DR$act_txt)
  }else if (d_case == 3) {
    name = paste0(name,'_DensityL_',action_DL$act_txt)
  }else if (d_case == 4) {
    name = paste0(name,'_DensityR_',action_DR$act_txt,'_DensityL_',action_DL$act_txt)
  }
  
  ##Adding height modif in name----
  if (h_case == 1) {
    name = paste0(name,'_Height_',action_HR$act_txt)
  }else if (h_case == 2) {
    name = paste0(name,'_HeightR_',action_HR$act_txt)
  }else if (h_case == 3) {
    name = paste0(name,'_HeightL_',action_HL$act_txt)
  }else if (h_case == 4) {
    name = paste0(name,'_HeightR_',action_HR$act_txt,'_HeightL_',action_HL$act_txt)
  }
  
  ##Adding selected IDs
  if (!is.null(select_ID)) {
    if (select_asExpression) {
      txt <- str_replace_all(select_ID,' ','')
      if (str_detect(txt,'/')) {
        txt <- str_replace_all(txt,'/','over')
      }
    } else {
      txt <- sprintf('on%.0fSegments',length(select_ID))
    }
    
    name = paste0(name,'_',txt)
  }
  
  new_namefile <- str_replace(namefile,'\\.',paste0(name,'.'))
  export_filename <- str_replace(filename,namefile,new_namefile)
  
  st_write(shapefile_save,export_filename,quiet = TRUE, append=FALSE)
  

  
  return(export_filename)
}






















