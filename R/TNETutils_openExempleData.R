

#' Get path to exemples files
#' 
#' This function will get all files needed for the exemples and return all path in a list. License info for the data used is in the details section
#'
#' @returns list with all path to all Exemple data and view license informations
#' @export
#' 
#' @param license if TRUE, License information of the data is shown. If false, all exemple file path are exported.
#' @details
#' J2000 Saône exemple data provided in this package are licensed under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1) and are provided 'AS IS', with no warranties.
#'
#' SAFRAN exemple data provided in this package are provided by "Météo-France" and are royalty-free under [Licence Ouverte d'Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2014/05/Licence_Ouverte.pdf)
#' 
#' @examples
#' exempleData <- TNETutils_openExempleData()
#' print(exempleData)
#' 
#' TNETutils_openExempleData(license = TRUE)
TNETutils_openExempleData <- function(license = FALSE) {
  
  if (license){
   folder =  system.file("extdata", package = "TnetRUI")
   
   cat('\033[0;31mJ2000 License file: \033[0m\n')
   J2K_license <- file.path(folder,'J2K_Ardiere','LICENSE')
   J2K_license_content <- readLines(J2K_license, warn = FALSE)
   cat(paste0(J2K_license_content,collapse='\n'))
   
   cat('\n\n\033[0;31mSAFRAN License file: \033[0m\n')
   SAFRAN_license <- file.path(folder,'Safran','LICENSE')
   SAFRAN_license_content <- readLines(SAFRAN_license, warn = FALSE)
   cat(paste0(SAFRAN_license_content,collapse='\n'))
   
  }else{
    results <- list(Shapefile_segments =  system.file("extdata","TestNetwork_Ardiere.shp", package = "TnetRUI"),
                    Shapefile_nodes = system.file("extdata","TestNetwork_ND_Ardiere.shp", package = "TnetRUI"),
                    DEM = system.file("extdata","DEM_Ardiere.tif", package = "TnetRUI"),
                    SAFRAN_grid = system.file("extdata","SAFRAN_grid_Ardiere.shp", package = "TnetRUI"),
                    J2K_path = system.file("extdata","J2K_Ardiere", package = "TnetRUI"))
    
    return(results)
  }
  
}