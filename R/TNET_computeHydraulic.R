#' @title Compute all hydraulic data from Q for T-NET calculations
#' @description Calculate rivers width (\eqn{Bm}), depth (\eqn{H}) and water travel time (\eqn{TPS}) with Discharge as input data, using EstimKart formulas. Rivers width can be recalibrate using observed width at a specific discharge quantile.
#' 
#' 
#' @param path_data Path to the folder containing Q created by [TNET_computeQ()]
#' @param shapefile Path to the shapefile with all info on segments. Columns needed are detailed in the details section.
#' @param calcul.Bm *Use only if `hydro_model = 'J2000'`* Method to calculate width: 'EK' use EstimKart, 'EKrecal' will use estimekart and recalibrate all segments in `shapefile_largeur` using discharge quantile in `quantile.calc`.
#' @param shapefile_largeur *Use only if `hydro_model = 'J2000'` and `calcul.Bm` = 'EKrecal'*. Path to a shapefile with all segments to recalibrate and the width. Columns needed are detailed in the details section.
#' @param quantile.calc *Use only if `hydro_model = 'J2000'` and `calcul.Bm` = 'EKrecal'*. Quantile of discharge to use for observed river width in `shapefile_largeur`.
#'
#' @return All hydraulic data (H, Bm, TPS) as NetCDF files in the folder `path_data`
#' 
#' @details
#' 
#' **If `hydro_model = 'J2000'`**
#' 
#' With J2000 hydro models, EstimKart formulas (see reference section) are use with the mean parameter values.
#' 
#' *River segment width (\eqn{Bm}):*
#' 
#' \deqn{Bm = ad \cdot (Qmean)^{bd} \cdot \left(\frac{Qaval}{Qmean}\right)^{b}}{Bm = ad * (Qmean^bd) * ((Qaval / Qmean)^b)}
#' 
#' with \eqn{ad = 7.482}, \eqn{bd = 0.477}, \eqn{b = 0.148}
#' 
#' *River segment depth (\eqn{H}):*
#' 
#' \deqn{H = cd \cdot (Qmean)^{fd} \cdot \left(\frac{Qaval}{Qmean}\right)^{f}}{H = cd * (Qmean^fd) * ((Qaval / Qmean)^f)}
#' 
#' with \eqn{cd = 0.340}, \eqn{fd = 0.259}, \eqn{f = 0.292}
#' 
#' *Water travel time in river segment (\eqn{TPS}):*
#' 
#' \deqn{TPS = \frac{Longueur\_m}{\left(\frac{Qaval}{H \cdot Bm}\right)} \div 3600}{TPS = (Longueur_m / (Qaval / (H * Bm))) / 3600}
#'
#'
#' where:
#' - \eqn{Longueur\_m}{Longueur_m} is the length of the river segment in meters.
#' - \eqn{Qaval} is the flow rate at the segment's outlet.
#' - \eqn{Qmean} is the mean flow rate.
#'
#' \cr
#' \cr
#' *\eqn{Bm} formulas use on recalibrate segments is the following:*
#' 
#' \deqn{Bm = largeur \cdot \left(\frac{Qaval}{Qquantile}\right)^{b}}{Bm = largeur * ((Qaval / Qquantile)^b)}
#' 
#' with \eqn{b = 0.148}
#' 
#' where:
#' - \eqn{largeur} is the width of the river segment in meters provided by `shapefile_largeur`.
#' - \eqn{Qquantile} is the flow quantile given at the quantile `quantile.calc`
#' 
#' 
#' *Columns needed in the `shapefile`*
#' 
#' |                              |                                           |
#' |------------------------------|-------------------------------------------|
#' | \eqn{gid\_new}{gid_new}      | ID of the Topage segment                  |
#' | \eqn{Longueur\_m}{Longueur_m}| Length of the river segment (in meter)    |
#' 
#' *Columns needed in the `shapefile_largeur`*
#' 
#' |                              |                                                    |
#' |------------------------------|----------------------------------------------------|
#' | \eqn{gid\_new}{gid_new}      | ID of the Topage segment                           |
#' | \eqn{largeur}{largeur}       | Width to recalibrate the river segment (in meter)  |
#'
#' 
#' **If 'hydro_model = 'EROS'**
#' 
#' With EROS hydrological model, EstimKart formaulas are used with parameter values computed for each river segments.
#' 
#' 
#' *EstimKart parameter*
#' 
#' Constant parameter:
#' 
#' ad0 = 2.122, adbv = 0, adslo = -0.076, adordr = 0
#' 
#' bd0 = 0.475, bdq = 0, bdbv = 0, bdslo = 0, bdordr = 0
#' 
#' b0 = 0.125, bq = 0, bbv = 0, bslo = 0, bordr = 0
#'
#' cd0 = -0.966, cdbv = 0, cdslo = -0.058, cdordr = 0
#' 
#' fd0 = 0.298, fdq = 0, fdbv = 0, fdslo = 0, fdordr = 0
#' 
#' f0 = 0.302, fq = 0, fbv = 0, fslo = 0, fordr = 0
#' 
#' \deqn{sSlope = \sqrt{slope \cdot 1000}}{sSlope = sqrt(slope * 1000)}
#' 
#' \deqn{ordre = Strahler order - 1}{ordre = Strahler order - 1}
#' 
#' 
#' For Width computation:
#' 
#' \deqn{ad = \exp\left(ad_0 + ad_{bv} \cdot \log(\text{Aire\_tronc}) + ad_{slo} \cdot sSlope + ad_{ordr} \cdot ordre\right)}{ad = exp(ad0 + adbv * log(Aire_tronc) + adslo * sSlope + adordr * ordre)}
#' 
#' \deqn{bd = bd0 + bdq \cdot log(Qmean) + bdbv \cdot log(\text{Aire\_tronc}) + bdslo \cdot sSlope + bdordr \cdot ordre}{bd = bd0 + bdq * log(Qmean) + bdbv * log(Aire_tronc) + bdslo * sSlope + bdordr * ordre}
#' 
#' \deqn{b  = b0 + bq \cdot log(Qmean) + bbv \cdot log(\text{Aire\_tronc}) + bslo \cdot sSlope + bordr \cdot ordre}{b  = b0 + bq * log(Qmean) + bbv * log(Aire_tronc) + bslo * sSlope + bordr * ordre}
#' 
#' For Height computation:
#' 
#' \deqn{cd = exp(cd0 + cdbv \cdot log(\text{Aire\_tronc}) + cdslo \cdot sSlope + cdordr \cdot ordre)}{cd = exp(cd0 + cdbv * log(Aire_tronc) + cdslo * sSlope + cdordr * ordre)}
#' 
#' \deqn{fd = fd0 + fdq \cdot log(Qmean) + fdbv \cdot log(\text{Aire\_tronc}) + fdslo \cdot sSlope + fdordr \cdot ordre}{fd = fd0 + fdq * log(Qmean) + fdbv * log(Aire_tronc) + fdslo * sSlope + fdordr * ordre}
#' 
#' \deqn{f  = f0 + fq \cdot log(Qmean) + fbv \cdot log(\text{Aire\_tronc}) + fslo \cdot sSlope + fordr \cdot ordre}{f  = f0 + fq * log(Qmean) + fbv * log(Aire_tronc) + fslo * sSlope + fordr * ordre}
#' 
#'
#' *River segment width (\eqn{Bm}):*
#' 
#' if \eqn{Bm} is set in the `shapefile`:
#' \deqn{Bm = Bm}
#' 
#' if \eqn{nw\_b > -1} or \eqn{L50\_corr > -1} or \eqn{Q50\_corr > -1} are set in the `shapefile`:
#' \deqn{Bm = L50\_corr \cdot \left(\frac{Qaval}{Q50\_corr}\right)^{nw_b}}{Bm = L50_corr * ((Qaval / Q50_corr)^nw_b)}
#' 
#' else:
#' \deqn{Bm = ad \cdot (Qmean)^{bd} \cdot \left(\frac{Qaval}{Qmean}\right)^{b}}{Bm = ad * (Qmean^bd) * ((Qaval / Qmean)^b)}
#' 
#' 
#' *River segment depth (\eqn{H}):*
#' 
#' if \eqn{nw\_f > -1} or \eqn{H50\_corr > -1} or \eqn{Q50\_corr > -1} are set in the `shapefile`:
#' \deqn{H = H50\_corr \cdot \left(\frac{Qaval}{Q50\_corr}\right)^{nw_f}}{H = H50_corr * ((Qaval / Q50_corr)^nw_f)}
#' 
#' else:
#' \deqn{H = cd \cdot (Qmean)^{fd} \cdot \left(\frac{Qaval}{Qmean}\right)^{f}}{H = cd * (Qmean^fd) * ((Qaval / Qmean)^f)}
#' 
#' 
#' *Water travel time in river segment (\eqn{TPS}):*
#' 
#' \deqn{TPS = \frac{Longueur\_m}{\left(\frac{Qaval}{H \cdot Bm}\right)} \div 3600}{TPS = (Longueur_m / (Qaval / (H * Bm))) / 3600}
#'
#'
#' *Columns needed in the `shapefile`*
#' 
#' |                              |                                                            |
#' |------------------------------|------------------------------------------------------------|
#' | \eqn{OBJECTID_1}{OBJECTID_1} | ID of the river segment                                    |
#' | \eqn{OSTRAHLER}{OSTRAHLER}   | Strahler order of the river segment                        |
#' | \eqn{pente2}{pente2}         | Slope of the river segment                                 |
#' | \eqn{longueur}{longueur}     | Length of the river segment                                |
#' | \eqn{nw\_b}{nw_b}            | EstmiKart b parameter recalibrate for the river segment    |
#' | \eqn{nw\_f}{nw_f}            | EstimKart f parameter recalibrate for the river segment    |
#' | \eqn{Q50\_corr}{Q50_corr}    | Median discharge to use with recalibrate EstimKart formulas|
#' | \eqn{H50\_corr}{H50_corr}    | Median height to use with recalibrate EstimKart formulas   |
#' | \eqn{L50\_corr}{L50_corr}    | Median width to use with recalibrate Estimkart formulas    |
#' | \eqn{Bm}{Bm}                 | Measured width of the river segments                       |
#' | \eqn{B\_mes}{B_mes}          | Another measured width of the river segment                |
#' | \eqn{Aire\_tronc}{Aire_tronc}| Drain area of the river segment                            |
#'
#'
#' @seealso [TNET_initializeSim()], [TNET_readJ2K()], [TNET_computeQ()]
#' 
#' @references 
#' Maxime Morel, Doug J. Booker, Frédéric Gob, Nicolas Lamouroux,
#' Intercontinental predictions of river hydraulic geometry from catchment physical characteristics,
#' Journal of Hydrology, \url{https://doi.org/10.1016/j.jhydrol.2019.124292}.
#' 
#' @examples
#' 
#' ## If we want to calculate width the normal way
#' TNET_computeHydraulic(path_data = path/to/data,
#'                       shapefile = path/to/shapefile.shp)
#'                       
#' ## If we want to calculate width and recalibrate some segments with width at quantile = 0.99
#' TNET_computeHydraulic(path_data = path/to/data,
#'                       shapefile = path/to/shapefile.shp,
#'                       calcul.Bm = 'EKrecal',
#'                       shapefile_largeur = path/to/shapefile_width.shp,
#'                       quantile.calc = 0.99)
#'
#'
#' ############################################################
#' ## Using this function with TNET_initializeSim() function ##
#' ############################################################
#' infoSimu <- TNET_initializeSim(...)
#' 
#' TNET_readJ2K(...) #Read ReachLoop.dat file
#' 
#' TNET_computeQ(...)
#' 
#' TNET_computeHydraulic(path_data = infoSimu$hydroPath, 
#'                       shapefile = infoSimu$TOPAGE_shape,
#'                       shapefile_largeur = infoSimu$Largeur_shape,
#'                       calcul.Bm =  infoSimu$Bm_method,
#'                       quantile.calc =  infoSimu$Bm_quantile)
#' 
#' @export
TNET_computeHydraulic <- function(path_data, shapefile, hydro_model = 'J2000', calcul.Bm = 'EK', shapefile_largeur = NULL, quantile.calc = NULL) {

  fnc_arg <- as.list(environment())

  if (is.null(hydro_model)) {
    hydro_model = 'J2000'
  }
  
  #Compute Discharge using different hydrological models----
  if (hydro_model == 'J2000'){
    results_file <- computeHydraulic_J2000(path_data, shapefile, calcul.Bm, shapefile_largeur, quantile.calc)
  } else if (hydro_model == 'EROS') {
    results_file <- computeHydraulic_EROS(path_data, shapefile)
  } else {
    stop("hydro_model can only be J2000 or EROS")
  }
  
  
  #Put info in NetCDF file----
  for (i in 1:length(results_file)) {
    #Put all info as NetCDF global argument
    nc_setAttributes(results_file[[i]],match.call()[[1]],fnc_arg)
    #close the NetCDF file
    nc_close(results_file[[i]])
  }
  
}





computeHydraulic_J2000 <- function(path_data, shapefile, calcul.Bm = 'EK', shapefile_largeur = NULL, quantile.calc = NULL) {
  # library(data.table)
  # library(stringr)
  # library(sf)
  # library(qs)
  # library(rlist)
  # library(parallel)
  
  # Start1 = tic()
  if(is.null(calcul.Bm)){calcul.Bm = 'EK'}
  nb_per_window = 1000
  
  #Working with all function arguments----
  if (!calcul.Bm %in% c('EK', 'EKrecal')) {
    stop('calcul.Bm must be "EK" or "EKrecal"')     
  }else if(calcul.Bm == 'EK') {
    cat('Largeur calculé avec la méthode EstimKart non modifié/n')
  }else if(calcul.Bm == 'EKrecal') {
    
    #Check if shapefile_largeur id provided
    if (is.null(shapefile_largeur))
    {
      stop("A path to the shapefile with measured width of rivers must be provided in 'shapefile_largeur'")
    }
    
    troncons_Lcalc <- setDT(st_read(shapefile_largeur,quiet = TRUE))
    troncons_transect <- troncons_Lcalc[,c("gid_new","largeur")]
    colnames(troncons_transect)[colnames(troncons_transect) == "largeur"] <- "W_transect"
    
    troncons_transect <- troncons_transect[!is.na(W_transect),]
    
    cat(sprintf('Largeur calculé avec la méthode EstimKart recalibré.\n\tLa largeur des %.0f tronçons de "shapefile_largeur" seront utilisées\n',
                nrow(troncons_transect)))
  }
  
  # file_Qb = file.path(path_data,'Qb.nc')
  
  
  #Reading Shapefile----
  topage_data <- st_read(shapefile,quiet=TRUE)
  if ("Longueur_m" %in% colnames(topage_data)){
    troncons <- topage_data[,c("gid_new","Longueur_m")] #, "Aire_tt", 'Pente'
  } else {
    troncons <- topage_data[,c("gid_new")] #, "Aire_tt", 'Pente'
    troncons$Longueur_m <- as.numeric(st_length(troncons))
  }
  
  # troncons <- troncons[,sSlope := sqrt(Pente * 1000)]
  nb_troncons <- nrow(troncons)
  
  # chunksize <- min(c(nb_troncons,100))
  
  # Create all netCDF export files ----
  
  file_Qaval = file.path(path_data,'Qaval.nc')
  
  Bm_file  <- TNETutils_createNetCDF(path_createdNetCDF = file.path(path_data,'Bm.nc'),
                                     variable = 'Bm',
                                     units = 'm',
                                     NetCDF_base = file_Qaval,
                                     ID_dim = troncons$gid_new,
                                     chunksize = 100)
  
  H_file   <- TNETutils_createNetCDF(path_createdNetCDF = file.path(path_data,'H.nc'),
                                     variable = 'H',
                                     units = 'm',
                                     NetCDF_base = file_Qaval,
                                     ID_dim = troncons$gid_new,
                                     chunksize = 100)
  
  TPS_file <- TNETutils_createNetCDF(path_createdNetCDF = file.path(path_data,'TPS.nc'),
                                     variable = 'TPS',
                                     units = 'h',
                                     NetCDF_base = file_Qaval,
                                     ID_dim = troncons$gid_new,
                                     chunksize = 100)
  
  #Running Hydraulic calculations----
  ##Constante EstimKart
  #largeur
  ad <- 7.482
  bd <- 0.477
  b <- 0.148
  #hauteur
  cd <- 0.340
  fd <- 0.259
  f <- 0.292
  
  
  #Setting number of segments calculated at each iterations
  All_id_TNET_start <- seq(1,nb_troncons,nb_per_window)
  if (length(All_id_TNET_start) > 1){
    All_id_TNET_end <- c(All_id_TNET_start[2:length(All_id_TNET_start)]-1,nb_troncons)
  } else {
    All_id_TNET_end <- nb_troncons
  }
  nb_window <- length(All_id_TNET_start)
  
  Chargement_Command_window(nb_window,'Calcul données hydraulique')
  for (i in 1:nb_window) {
    
    ##Lecture des débits----
    troncons_study <- troncons[All_id_TNET_start[i]:All_id_TNET_end[i],]
    
    Data <- TNETresult_read(filename = file_Qaval,
                             ID = troncons_study$gid_new,
                             variable_name = 'Qaval',
                             format_long = TRUE)
    
    # Data$gid_new <- as.numeric(Data$gid)
    # Data[,gid:=NULL]
    
    #Calcul des statistiques (Moyenne et quantile)----
    Data_mean = Data[,.(Qmean = mean(Qaval)),
                     by = gid]
    
    if (calcul.Bm == 'EKrecal') {
      Data_quantile <- Data[,.(Qquantile = quantile(Qaval,quantile.calc)),by = gid]
      
      Data_mean <- merge(Data_mean,Data_quantile, by='gid')
    }
    
    Data_mean <- merge(Data_mean,troncons_study,by.x = 'gid',by.y = 'gid_new')
    
    Data_Q <- merge(Data,Data_mean,by='gid',all.x = TRUE)
    
    #On calcul la largeur journalière----
    if (calcul.Bm == 'EKrecal') {
      #On calcul différemment les tronçons avec des transects
      Data_Qtransect <- Data_Q[gid %in% troncons_transect$gid_new]
      Data_Q  <- Data_Q[!gid %in% troncons_transect$gid_new]
      
      #Calcul avec EstimKart quand on a pas les transect
      Data_Q <- Data_Q[, Bm := ad * (Qmean)^bd * (Qaval/Qmean)^b]
      Data_Q[,Qquantile := NULL]
      
      #Calcul avec recalibration quand les transect sont disponible
      Data_Qtransect <- merge(Data_Qtransect,troncons_transect,by.x='gid',by.y = 'gid_new',all.x = TRUE)
      
      Data_Qtransect <- Data_Qtransect[, Bm := W_transect * (Qaval/Qquantile)^b]
      Data_Qtransect[,W_transect := NULL]
      Data_Qtransect[,Qquantile := NULL]
      
      #On merge les résultats
      Data_Q <- rbind(Data_Q,Data_Qtransect)
      
    }else{
      #On calcul la largeur journalière par EstimKart
      Data_Q <- Data_Q[, Bm := ad * (Qmean)^bd * (Qaval/Qmean)^b]
    }
    
    #Calcul de la hauteur----
    Data_Q <- Data_Q[, H := cd * ((Qmean)^fd) * ((Qaval/Qmean)^f)]
    
    #Calcul du temps passé dans le tronçon----
    Data_Q <- Data_Q[,TPS := (Longueur_m/(Qaval/(H*Bm)))/3600]
    #Calcul du Débit latéral souterrain / surface mouillé----
    # DataQb <- TNETresult_read(filename = file_Qb,
    #                          ID = troncons_study$gid_new,
    #                          variable_name = 'Qb',
    #                          format_long = TRUE)
    # DataQb$gid_new <- as.numeric(DataQb$gid)
    # DataQb[,gid:=NULL]
    # Data_Q <- merge(Data_Q,DataQb,by=c('Date' ,'gid_new'),all.x = TRUE)
    # Data_Q <- Data_Q[,Qbms := (Longueur_m/(Qaval/(H*Bm)))/3600]
    # 
    
    
    #Export des résultats----
    #Add to file
    TNETutils_appendNetCDF(Bm_file,Data_Q)
    TNETutils_appendNetCDF(TPS_file,Data_Q)
    TNETutils_appendNetCDF(H_file,Data_Q)
    
     
    Chargement_Command_window()
  }
  
 return(list(Bm_file,H_file,TPS_file)) 
}





computeHydraulic_EROS <- function(path_data, shapefile) {
  
  nb_per_window = 1000
  
  #reading shapefile----
  topage_data <- st_read(shapefile,quiet=TRUE)
  columns_to_keep <- c("OBJECTID_1","OSTRAHLER","pente2","longueur","nw_b","nw_f",
                       "Q50_corr","H50_corr","L50_corr","Bm","B_mes","Aire_tronc",
                       "pkAmontNF")#,"air_strNIS","air_strNFS","air_unit"
  
  troncons <- topage_data[,columns_to_keep] 
  
  ##Add columns to compute
  troncons$sSlope <- sqrt(troncons$pente2 * 1000)
  troncons$long_m <- troncons$longueur * 1000
  troncons$ordre <- troncons$OSTRAHLER - 1
  troncons$Bm_Manning <- troncons$Bm
  
  troncons$pente2 <- NULL
  troncons$longueur <- NULL
  troncons$OSTRAHLER <- NULL
  troncons$Bm <- NULL
  
  nb_troncons <- nrow(troncons)
  
  # Create all netCDF export files ----
  ##Reading Q netCDF----
  file_Qaval = file.path(path_data,'Qaval.nc')
  
  Bm_file  <- TNETutils_createNetCDF(path_createdNetCDF = file.path(path_data,'Bm.nc'),
                                    variable = 'Bm',
                                    units = 'm',
                                    NetCDF_base = file_Qaval,
                                    ID_dim = troncons$OBJECTID_1,
                                    chunksize = 100)
  H_file   <- TNETutils_createNetCDF(path_createdNetCDF = file.path(path_data,'H.nc'),
                                    variable = 'H',
                                    units = 'm',
                                    NetCDF_base = file_Qaval,
                                    ID_dim = troncons$OBJECTID_1,
                                    chunksize = 100)
  TPS_file <- TNETutils_createNetCDF(path_createdNetCDF = file.path(path_data,'TPS.nc'),
                                    variable = 'TPS',
                                    units = 'h',
                                    NetCDF_base = file_Qaval,
                                    ID_dim = troncons$OBJECTID_1,
                                    chunksize = 100)
  
  
  
  #Initialize EstimKart parameter----
  ##cd parameter
  cd0 = -0.966
  cdbv = 0
  cdslo = -0.058
  cdordr = 0
  
  ##fd parameter
  fd0 = 0.298
  fdq = 0
  fdbv = 0
  fdslo = 0
  fdordr = 0
  
  ##f parameter
  f0 = 0.302
  fq = 0
  fbv = 0
  fslo = 0
  fordr = 0
  
  ##ad parameter
  ad0 = 2.122
  adbv = 0
  adslo = -0.076
  adordr = 0
  
  ##bd parameter
  bd0 = 0.475
  bdq = 0
  bdbv = 0
  bdslo = 0
  bdordr = 0
  
  ##b parameter
  b0 = 0.125
  bq = 0
  bbv = 0
  bslo = 0
  bordr = 0
  
  
  #Initialize loop on computations----
  #Setting number of segments calculated at each iterations
  All_id_TNET_start <- seq(1,nb_troncons,nb_per_window)
  if (length(All_id_TNET_start) > 1){
    All_id_TNET_end <- c(All_id_TNET_start[2:length(All_id_TNET_start)]-1,nb_troncons)
  } else {
    All_id_TNET_end <- nb_troncons
  }
  nb_window <- length(All_id_TNET_start)
  
  Chargement_Command_window(nb_window,'Calcul données hydraulique')
  for (i in 1:nb_window) {
    
    ##Opening discharge data----
    troncons_study <- setDT(st_drop_geometry(troncons[All_id_TNET_start[i]:All_id_TNET_end[i],]))
    #troncons_study <- setDT(st_drop_geometry(troncons[troncons$OBJECTID_1 == 49014,]))
    
    Data <- TNETresult_read(filename = file_Qaval,
                             ID = troncons_study$OBJECTID_1,
                             variable_name = 'Qaval',
                             format_long = TRUE)
    
    
    #Cut segments into Recalibrate (new) or usual (old) EstimKart formulas----
    new_Ek <- troncons_study$nw_b > -1 | troncons_study$nw_f > -1 | troncons_study$L50_corr > -1 | troncons_study$H50_corr > -1 |  troncons_study$Q50_corr > -1
    
    troncon_newEK <- troncons_study[new_Ek,c('OBJECTID_1','pkAmontNF','B_mes','Bm_Manning','nw_b','nw_f','L50_corr','H50_corr','Q50_corr','long_m')]
    troncon_oldEK <- troncons_study[!new_Ek,c('OBJECTID_1','pkAmontNF','Aire_tronc','Bm_Manning','sSlope','ordre','long_m')]
    
   
    #Compute segments with recalibrate EstimKart formulas----
    if (nrow(troncon_newEK) > 0) {
      Data_newEK <- merge(Data,troncon_newEK,by.x = 'gid',by.y = 'OBJECTID_1')
      
      ##compute width Bm----
      Data_newEK[(Bm_Manning >  B_mes) & (Bm_Manning > 0), Bm := Bm_Manning]
      Data_newEK[(Bm_Manning <= B_mes) & (Bm_Manning > 0) ,Bm := B_mes]
      Data_newEK[Bm_Manning <= 0 ,Bm := L50_corr*((Qaval/Q50_corr)^nw_b)]
      
      ##compute height H----
      Data_newEK[,H := H50_corr*((Qaval/Q50_corr)^nw_f)]
    }else{
      #Put nothing with good columns if there is no "new" Estimekart formulas
      Data_newEK <- Data[gid %in% troncon_oldEK$OBJECTID_1,]
      Data_newEK[,Bm := NA]
      Data_newEK[,H := NA]
      Data_newEK[,long_m := NA]
      Data_newEK[,pkAmontNF := NA]
    }
    
    #Compute segments with usual EstimKart formulas----
    if (nrow(troncon_oldEK) > 0) {
      Data_oldEK <- Data[gid %in% troncon_oldEK$OBJECTID_1,]
      DataMean <- Data_oldEK[,.(Qmean = mean(Qaval)),by='gid']
      
      ##compute Estimkart parameters----
      #Height computation
      troncon_oldEK <- merge(troncon_oldEK,DataMean, by.x = 'OBJECTID_1', by.y = "gid")
      troncon_oldEK[,cd := exp(cd0 + cdbv * log(Aire_tronc) + cdslo * sSlope + cdordr * ordre)]
      troncon_oldEK[,fd := fd0 + fdq * log(Qmean) + fdbv * log(Aire_tronc) + fdslo * sSlope + fdordr * ordre]
      troncon_oldEK[,f  := f0 + fq * log(Qmean) + fbv * log(Aire_tronc) + fslo * sSlope + fordr * ordre]
      #Width computation
      troncon_oldEK[,ad := exp(ad0 + adbv * log(Aire_tronc) + adslo * sSlope + adordr * ordre)]
      troncon_oldEK[,bd := bd0 + bdq * log(Qmean) + bdbv * log(Aire_tronc) + bdslo * sSlope + bdordr * ordre]
      troncon_oldEK[,b  := b0 + bq * log(Qmean) + bbv * log(Aire_tronc) + bslo * sSlope + bordr * ordre]
      
      Data_oldEK <- merge(Data_oldEK,troncon_oldEK,by.x = 'gid',by.y = 'OBJECTID_1')
      
      #compute width Bm----
      Data_oldEK[Bm_Manning > 0 ,Bm := Bm_Manning]
      Data_oldEK[Bm_Manning <= 0 ,Bm := ad * (Qmean^bd) * ((Qaval/Qmean)^b)]
      
      #compute Height H----
      Data_oldEK[Bm_Manning  > 0 ,H := (Qaval / (Bm*40*sSlope))^(3/5)]
      Data_oldEK[Bm_Manning <= 0 ,H := (cd * (Qmean^fd) * ((Qaval/Qmean)^f)) + 0.1]
      
      
    }else{
      #Put nothing with good columns if there is no "old" Estimekart formulas
      Data_oldEK <- Data[gid %in% troncon_oldEK$OBJECTID_1,]
      Data_oldEK[,Bm := NA]
      Data_oldEK[,H := NA]
      Data_oldEK[,long_m := NA]
      Data_oldEK[,pkAmontNF := NA]
    }
    
    #Merge Old and new EstimKart computations----
    Data_result <- rbind(Data_newEK[,c("gid","Date","Qaval","Bm","H",'pkAmontNF',"long_m")],Data_oldEK[,c("gid","Date","Qaval","Bm","H",'pkAmontNF',"long_m")])
    
    Data_result$Bm[Data_result$pkAmontNF > 200] <- Data_result$Bm[Data_result$pkAmontNF > 200] *1.5
    #Compute Speed and Time ----
    Data_result[,CV:=Qaval/(Bm* H)]
    Data_result[,TPS:=(long_m/CV/3600)]
    
    #Add to file
    TNETutils_appendNetCDF(Bm_file,Data_result)
    TNETutils_appendNetCDF(TPS_file,Data_result)
    TNETutils_appendNetCDF(H_file,Data_result)
    
    Chargement_Command_window()
  }
  
  return(list(Bm_file,H_file,TPS_file)) 
}

























