
#' @title Read information in the header of a J2K .dat file
#' @description
#' Will read the first 20 line of ReachLoop.dat or HRULoop.dat created by J2000 in order to get informations on the J2K simulation
#' 
#'
#' @param path_J2K_datFile path to the J2K .dat file to get the information
#' @param date.start POSIXct. date to start reading the file. **ONLY ON LINUX**
#' 
#' @return a list containing all file info describe in detals section.
#' @export
#' 
#' @details
#' The data read in the header is the following:
#' - `ind_startData`: Line where the data begin in the file
#' - `dateStart`: first day of the J2000 simulation
#' - `dateEnd`: last day of the simulation
#' - `Nbre_date`: number of days in the simulation
#' - `colNames`: all names of the columns
#' - `Nbre_col`: number of columns in the .dat file
#' 
#'
TNETutils_getDatInfoJ2K <- function(path_J2K_datFile, date.start = NULL) {
  # library(stringr)
  
  con <- file(path_J2K_datFile,"r")
  start_file <- readLines(con,n=20)
  All_col_names <-unlist(str_split(start_file[7],'\t'))
  while(All_col_names[length(All_col_names)] == ""){
    All_col_names <- All_col_names[1:(length(All_col_names)-1)]
  }
  
  info_Time_ind = which(start_file == '@ancestors')+1
  line_infoTimeLoop = str_split(start_file[info_Time_ind],'\t')[[1]]
  if (line_infoTimeLoop[2] == "TimeLoop") {
    Nb_date = as.numeric(line_infoTimeLoop[3])
  } else {
    Nb_date = NA
  }
  
  start_data_ind = which(start_file == '@start')
  
  line_date <- start_file[start_data_ind-1]%>%
    str_remove('TimeLoop\t')%>%
    str_split(' ')%>%
    unlist()
  
  date_start <-  as.POSIXct(paste( line_date[1],line_date[2]),
                            format = "%Y-%m-%d %H:%M",
                            tz = "GMT")
  date_end <- date_start + days(Nb_date -1)
  close(con)
  
  #Try to use grep to read only necessaries lines
  if (Sys.info()["sysname"] == "Linux" & !is.null(date.start)) {
    date_infile <- format(date.start,'%Y-%m-%d %H:%M')
    # Utiliser grep sous Linux
    result <- system(paste0("grep -n '",date_infile,"' ",path_J2K_datFile), intern = TRUE)
    
    text_line <-  paste0(":TimeLoop\t",date_infile)
    if (grepl(text_line, result)) {
      line <- as.numeric(gsub(text_line,'',result))
      
      Nb_date = length(seq(date.start, date_end, by="days"))
      date_start = date.start
      start_data_ind <- line + 1 #to get @start just after
    }
  }
  
  
  
  
  
  result <- list("ind_startData" = start_data_ind,
                 "dateStart" = date_start,
                 "dateEnd" = date_end,
                 "Nbre_date" = Nb_date,
                 "colNames" = All_col_names,
                 "Nbre_col" = length(All_col_names))
  return(result)
  
}


