#' @title Compare 2 NetCDF files created by T-NET on a periode of time
#' 
#' @description
#' This function will compare 2 NetCDF with the same variable created by 2 runs of 
#' TNET in order to put in evidence the difference between the 2 files in a shapefile
#' 
#' **All shapefiles used to run T-NET will be read, so be sure to keep them**
#' 
#'
#' @param NetCDF_reference Path to a NetCDF file created by TNET to used as a reference
#' @param NetCDF_toCompare Path to a NetCDF file created by TNET to compare
#' @param export_file Path to the shapefile to create with results
#' @param date *string or POSIXct*. Date where the comparison will take place (24 hours are taken). if `NULL`, `date.start` and `date.end` must be set.
#' @param date.start *string or POSIXct*. Date where the comparison will will start. If `NULL`, `date` must be set.
#' @param date.end *string or POSIXct*. Date where the comparison will end. if `NULL`, `date` must be set.
#' @param folder_shapefile Path to the folder where the shapefile used for the TNET calculation of both `NetCDF_reference` and `NetCDF_toCompare`. If `NULL`, the argument `simulation_path_shapeDirectory` created by TNET in each NetCDF file is used.
#' @param column.keep Column in both shapefile to be kept. If values in both shapefiles are différentes, a column "diff" is created with the value \eqn{diff = toCompare - reference}. To see all possible columns name, see [TNET_createShape()]
#'
#' @return Shapefile if `export_file = NULL`
#' @export
#' 
#' @details
#' The calculations are the following:
#' - If `date` is set, `date.start = date` and `date.end = date + hours(23)`
#' - Open both shapefiles used by T-NET to create `NetCDF_reference` and `NetCDF_toCompare`. Select columns set in `column.keep`
#' - Reading data of `NetCDF_reference` and `NetCDF_toCompare` on all IDs between `date.start` and `date.end` using [TNETresult_read()].
#' - Calculate mean value for each IDs for both data
#' - Calculate \eqn{diff = toCompare - reference}.
#' - Save shapefile.
#' 
#' @seealso [TNETresult_read()]
#' @examples
#' 
#' ## Exemple: Compair 2 ripisylve density scenarios ##
#' 
#' #Create simulation 
#' infoSimu <- TNET_initializeSim(...)
#' 
#' #Create Shapefile
#' TNET_createShape(...)
#' 
#' #Run TNET
#' TNET_run(infoSimu,
#'          ...)
#' 
#' # --> Be careful to rename the file created by T-NET, or it will be 
#' # erased at the next run of TNET: here the new name is Tw_NFS_VEG_ref.nc
#' 
#' #Change the vegetation density
#' infoSimu$shapeName <- TNET_modifyVeget(filename = infoSimu$shapeName,
#'                                        density = +30)
#' 
#' # Re-run TNET
#' TNET_run(infoSimu,
#'          ...)
#'          
#' # --> Here the file name is set at Tw_NFS_VEG_Density_+30.nc
#' 
#' 
#' #Compare both files on a day (between 2018-08-01 00:00 to 2018-08-01 23:00)
#' TNETresult_compare(NetCDF_reference = file.path(infoSimu$TNET_result_Path,"Tw_NFS_VEG_ref.nc"),
#'                    NetCDF_toCompare = file.path(infoSimu$TNET_result_Path,"Tw_NFS_VEG_Density_+30.nc"),
#'                    date = "2018-08-01",
#'                    column.keep = c("OSTRAHL", "vegPctL", "vegPctR"))
#'                    
#' #Compare both files on a month (between 2018-08-01 00:00 to 2018-09-11 00:00)
#' TNETresult_compare(NetCDF_reference = file.path(infoSimu$TNET_result_Path,"Tw_NFS_VEG_ref.nc"),
#'                    NetCDF_toCompare = file.path(infoSimu$TNET_result_Path,"Tw_NFS_VEG_Density_+30.nc"),
#'                    date.start = "2018-08-01",
#'                    date.end = "2018-09-01")
#'                    
#'                    

TNETresult_compare <- function(NetCDF_reference, NetCDF_toCompare, export_file = NULL, date = NULL, date.start = NULL, date.end = NULL,add_data = NULL, folder_shapefile = NULL, column.keep = NULL) {
  
  #Get value of arguments----
  if (is.null(date) & is.null(date.start) & is.null(date.end)) {
    stop('At least one of "date" or "date.start" and "date.end" arguments must be set')
  }
  
  if (!is.null(date.end) & is.null(date.start)) {
    stop("if date.start argument is set, date.end must be set too. If you want to calculate on a day, use date argument instead") 
  }
  if (!is.null(date.start) & is.null(date.end)) {
    stop("if date.end argument is set, date.start must be set too. If you want to calculate on a day, use date argument instead") 
  }
  
  
  
  
  #Get the date----
  if (!is.null(date) | !is.null(date.start) | !is.null(date.end)){
    #If we want to modify vegetation dentity
    if (!is.null(date) & is.null(date.start) & is.null(date.end)) {
      #If date is set, it will be use for date.start and date.end
      if (is.character(date)){
        date.start  <- text_toDate(date)
        date.end <- text_toDate(date) + hours(23)
      }else{
        date.start  <- date
        date.end <- date + hours(23)
      }
      
      date_str <- paste0(year(date.start),"_",month(date.start),"_",day(date.start))
    } else  if (!is.null(date.start) & !is.null(date.end)) {
      #If date.start and date.end are set, there are used
      if (is.character(date.end)){
        date.end <- text_toDate(date.end)
      }
      if (is.character(date.start)){
        date.start <- text_toDate(date.start)
      }
      
      date_str <- paste0(year(date.start),"_",month(date.start),"_",day(date.start),"_",hour(date.start),"h",minute(date.start),"_to_",year(date.end),"_",month(date.end),"_",day(date.end),"_",hour(date.end),"h",minute(date.end))
    }
  }
  
  #Verify NetCDF variables----
  nc_file_ref <- nc_open(NetCDF_reference)
  nVars <- nc_file_ref$nvars
  if (nVars == 1) { 
    variable_name_ref <- nc_file_ref$var[[1]]$name
    if (variable_name_ref == "data") {
      variable_name_ref = ncatt_get(nc_file_ref, varid = "data", attname = "variable_name")$value
    }
  }
  nc_close(nc_file_ref)
  
  
  nc_file_cmp <- nc_open(NetCDF_toCompare)
  nVars <- nc_file_cmp$nvars
  if (nVars == 1) { 
    variable_name_cmp <- nc_file_cmp$var[[1]]$name
    if (variable_name_cmp == "data") {
      variable_name_cmp = ncatt_get(nc_file_cmp, varid = "data", attname = "variable_name")$value
    }
  }
  nc_close(nc_file_cmp)
  
  if (variable_name_ref != variable_name_cmp) {
    stop(paste0("You are trying to compare 2 files that doesn't have the same variable, NetCDF_reference as ",variable_name_ref," and NetCDF_toCompare as ",variable_name_cmp))
  }
  
  
  #Get shapefile from NetCDF arguments----

  # shapefile_ref <- st_read(getShape(NetCDF_reference), quiet = TRUE)
  # shapefile_cmp <- st_read(getShape(NetCDF_toCompare), quiet = TRUE)
  
  shapefile_ref <- TNETutils_getShape_fromNETCDF(NetCDF_reference, folder_shapefile = folder_shapefile)
  shapefile_cmp <- TNETutils_getShape_fromNETCDF(NetCDF_toCompare, folder_shapefile = folder_shapefile)
  
  
  ##Compare shape----
  if (!is.null(column.keep)) {
    for (i in 1:length(column.keep)){
      colname <- column.keep[i]
      
      part_ref <- st_drop_geometry(shapefile_ref[,c('gid',colname)])
      colnames(part_ref)[colnames(part_ref) == colname] <- 'ref'
      
      part_cmp <- st_drop_geometry(shapefile_cmp[,c('gid',colname)])
      colnames(part_cmp)[colnames(part_cmp) == colname] <- 'cmp'
      
      part <- setDT(merge(part_ref,part_cmp))
      
      if (all(na.omit(part$ref == part$cmp))) {
        part[,cmp :=NULL]
        colnames(part)[colnames(part) == 'ref'] <- colname
      }else {
        part <- part[,diff := cmp - ref]
        colnames(part)[colnames(part) == 'ref'] <- paste0(colname,'_ref')
        colnames(part)[colnames(part) == 'cmp'] <- paste0(colname,'_cmp')
        colnames(part)[colnames(part) == 'diff'] <- paste0(colname,'_diff')
      }
      
      if (i==1) {
        shapefile = part
      }else {
        shapefile = merge(shapefile, part, by='gid')
      }
    }
    
    #add geom
    shapefile <- merge(shapefile,shapefile_ref[,'gid'])
  }else{
    shapefile <- shapefile_ref[,'gid']
  }
  
  
  
  #Read data to add----
  if (!is.null(add_data)){
    for (i in 1:length(add_data)) {
      file <- add_data[i]
      
      data <- TNETresult_read(filename = file, date_start = date.start, date_end = date.end, format_long = TRUE)
      data_var <- colnames(data)[3]
      colnames(data)[2] <- "gid"
      colnames(data)[3] <- "data"
      data <- data[,.(res = mean(data)), by=gid]
      colnames(data)[colnames(data) == "res"] <- data_var
      
      shapefile <- merge(shapefile,data, by = "gid")
    }
  }
  
  
  
  #Read data from NetCDF----
  #read reference data 
  data_ref <- TNETresult_read(filename = NetCDF_reference, date_start = date.start, date_end = date.end, format_long = TRUE)
  colnames(data_ref)[colnames(data_ref) == variable_name_ref] <- "data"
  data_ref <- data_ref[,.(data_ref = mean(data)), by=gid]
  
  #read to Compare data
  data_cmp <- TNETresult_read(filename = NetCDF_toCompare, date_start = date.start, date_end = date.end, format_long = TRUE)
  colnames(data_cmp)[colnames(data_cmp) == variable_name_cmp] <- "data"
  data_cmp <- data_cmp[,.(data_cmp = mean(data)), by=gid]
  
  
  
  #Compare data from NetCDF----
  data <- merge(data_ref,data_cmp, by=c('gid'))
  data <- data[,diff := data_cmp-data_ref]
  data_shape <- merge(shapefile,data, by = "gid")
  
  data_shape$date <- date_str
  # # Créer la carte avec ggplot2
  # map_plot <- ggplot(data_shape) +
  #   geom_sf(aes(size = OSTRAHL, color = diff), show.legend = TRUE) +
  #   scale_color_gradient2(low = "#2d22c9", mid = "#f5fa91", high = "#c90c3f", 
  #                         midpoint = 0, limits = c(-2.5, 2.5), 
  #                         name = "Différence") +
  #   scale_size_continuous(range = c(min(data_shape$OSTRAHL), max(data_shape$OSTRAHL)), name = "OSTRAHL") + # Augmenter la plage de taille
  #   theme_minimal() +
  #   theme(legend.position = "right") +
  #   labs(title = "Carte des différences avec épaisseur des lignes par OSTRAHL",
  #        color = "Différence", size = "OSTRAHL")
  # 
  # # Afficher la carte
  # print(map_plot)
  # 
  # 
  # # Créer la carte avec ggplot2
  # map_plot <- ggplot(data_shape) +
  #   geom_sf(aes(size = OSTRAHL), show.legend = TRUE) +
  #   # scale_color_gradient2(low = "#2d22c9", mid = "#f5fa91", high = "#c90c3f", 
  #   #                       midpoint = 19, limits = c(14, 24), 
  #   #                       name = "Tw") +
  #   # scale_size_continuous(range = c(min(data_shape$OSTRAHL), max(data_shape$OSTRAHL)), name = "OSTRAHL") + # Augmenter la plage de taille
  #   scale_size_identity() +
  #   # theme_minimal() +
  #   theme(legend.position = "right") +
  #   labs(title = "Carte des différences avec épaisseur des lignes par OSTRAHL",
  #        color = "Tw (°C)", size = "OSTRAHL")
  # 
  # # Afficher la carte
  # print(map_plot)
  # 
  
  #Save shape----
  string_identicalPart <- function(stringA, stringB) {
    
    # split strings into individual characters
    
    split_stringA <- unlist(strsplit(stringA, split =  ""))
    split_stringB <- unlist(strsplit(stringB, split =  ""))
    
    
    # pad vectors of unequal lengths with NAs
    
    if (length(split_stringA) < length(split_stringB)) {
      identical_A <- c(split_stringA, rep(NA, 
                                          (length(split_stringB)-length(split_stringA))))
      
      identical_B <- split_stringB
      
    } else { 
      identical_B <- c(split_stringB,rep(NA,
                                         (length(split_stringA)-length(split_stringB)))) 
      
      identical_A <- split_stringA
    }
    
    
    # compare padded vectors
    compared <- identical_A == identical_B
    
    # remove NAs left from padding
    compared <- na.omit(compared)
    
    # use names attribute to preserve information about positions 
    names(compared) <- seq(1, length(compared))
    
    # split comparison based on TRUE/FALSE switches
    list_indices <- split(compared, cumsum(c(0,diff(compared)!=0)))
    
    # define function to replace logicals with positions
    pick_indices <-  function(vec) {
      
      replaced <- if (sum(vec)!=0) {
        
        # replace TRUEs with positions preserved in names 
        
        as.numeric(names(vec)) } else {
          
          # set FALSEs to NULL
          
          NULL}
      return(replaced)
      
    }
    
    # apply the function to get a list of valid positions
    picker_list <- lapply(list_indices, pick_indices)
    
    # remove NULL matches
    picker_list_clean <- Filter(Negate(is.null), picker_list)
    
    
    # define function to select matching characters from original strings
    selector_helper <-  function(x, string) string[x]
    
    # apply the helper function to either of the strings
    temp_result <- lapply(picker_list_clean, selector_helper, split_stringA)
    
    # collapse the matching characters back into strings
    result <- sapply(temp_result, paste, collapse = "")
    
    # can insert result <- trimws(result) if spaces should not be in the output
    
    result <- result[nchar(result) == max(nchar(result))]
    
    names(result) <- NULL
    
    
    return(result)
    
  }
  
  if (is.null(export_file)){
    return(data_shape)
  } else {
    st_write(data_shape,export_file,quiet = TRUE,append=FALSE)
  }
  
  
 
}





























