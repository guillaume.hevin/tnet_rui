

#' @title Get all SAFRAN gris ID info from shapefile
#' @description
#' Get all info of SAFRAN grid where all river segments are located from the T-NET readable format to a better R understanding format.
#' 
#' @param shapefile Path to the shapefile with all info on segments. It must contain the following columns:
#' |                              |                                                                                                                                  |
#' |------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
#' | \eqn{gid\_new}{gid_new}      | ID of the Topage segment                                                                                                         |
#' | \eqn{ID\_M\_1}{ID_M_1}       | ID of the SAFRAN grid n°1 where the segment is located                                                                           |
#' | \eqn{ID\_M\_2}{ID_M_2}       | ID of the SAFRAN grid n°2 where the segment is located                                                                           |
#' | \eqn{ID\_M\_3}{ID_M_3}       | ID of the SAFRAN grid n°3 where the segment is located                                                                           |
#' | \eqn{ID\_M\_4}{ID_M_4}       | ID of the SAFRAN grid n°4 where the segment is located                                                                           |
#' | \eqn{ID\_M\_5}{ID_M_5}       | ID of the SAFRAN grid n°5 where the segment is located                                                                           |
#' | \eqn{ID\_M\_6}{ID_M_6}       | ID of the SAFRAN grid n°6 where the segment is located                                                                           |
#' | \eqn{ID\_M\_7}{ID_M_7}       | ID of the SAFRAN grid n°7 where the segment is located                                                                           |
#' | \eqn{ID\_M\_8}{ID_M_8}       | ID of the SAFRAN grid n°8 where the segment is located                                                                           |
#' | \eqn{Rap\_M\_1}{Rap_M_1}     | Part of the segment located in SAFRAN grid n°1                                                                                   |
#' | \eqn{Rap\_M\_2}{Rap_M_2}     | Part of the segment located in SAFRAN grid n°2                                                                                   |
#' | \eqn{Rap\_M\_3}{Rap_M_3}     | Part of the segment located in SAFRAN grid n°3                                                                                   |
#' | \eqn{Rap\_M\_4}{Rap_M_4}     | Part of the segment located in SAFRAN grid n°4                                                                                   |
#' | \eqn{Rap\_M\_5}{Rap_M_5}     | Part of the segment located in SAFRAN grid n°5                                                                                   |
#' | \eqn{Rap\_M\_6}{Rap_M_6}     | Part of the segment located in SAFRAN grid n°6                                                                                   |
#' | \eqn{Rap\_M\_7}{Rap_M_7}     | Part of the segment located in SAFRAN grid n°7                                                                                   |
#' | \eqn{Rap\_M\_8}{Rap_M_8}     | Part of the segment located in SAFRAN grid n°8                                                                                   |
#' 
#' @return data.table with for each segment, all SAFRAN grid traversed and the part of it in it.
#' @export
#' 
#' @examples
#' # example code
#' 
#' #Shapefile name
#' file <- path/to/shapefile.shp
#' 
#' read_sf(file)
#' #> | gid_new | ID_M_1 | ID_M_2 | ID_M_3 | ID_M_4 | ID_M_5 | ID_M_6 | ID_M_7 | ID_M_8 | Rap_M_1 | Rap_M_2 | Rap_M_3 | Rap_M_4 | Rap_M_5 | Rap_M_6 | Rap_M_7 | Rap_M_8|
#' #> |---------|--------|--------|--------|--------|--------|--------|--------|--------|---------|---------|---------|---------|---------|---------|---------|--------|
#' #> |145      |6331    |6332    |NaN     |NaN     |NaN     |NaN     |NaN     |NaN     |0.65     |0.35     |NaN      |NaN      |NaN      |NaN      |NaN      |NaN     |
#' #> |189      |6330    |NaN     |NaN     |NaN     |NaN     |NaN     |NaN     |NaN     |1        |NaN      |NaN      |NaN      |NaN      |NaN      |NaN      |NaN     |
#' #> |160195   |7852    |7856    |7851    |NaN     |NaN     |NaN     |NaN     |NaN     |0.42     |0.18     |0.4      |NaN      |NaN      |NaN      |NaN      |NaN     |
#' 
#' safran <- TNETutils_getSAFRAN_fromShape(file)
#' 
#' safran
#' #> | gid_new | SAFRAN | Rapport  |
#' #> |---------|--------|----------|
#' #> | 145     |  6331  |  0.65    |
#' #> | 145     |  6332  |  0.35    |
#' #> | 189     |  6330  |  1       |
#' #> | 160195  |  7852  |  0.42    |
#' #> | 160195  |  7856  |  0.18    |
#' #> | 160195  |  7851  |  0.4     |
#' 
TNETutils_getSAFRAN_fromShape <- function(shapefile) {

  if (is.character(shapefile)) {
    troncons <- setDT(st_read(shapefile,quiet=TRUE))
  } else {
    troncons <- setDT(shapefile)
  }
  
  
  if('ID_M_1' %in% colnames(troncons)) {
    col_id <- c('gid_new','ID_M_1','ID_M_2','ID_M_3','ID_M_4','ID_M_5','ID_M_6','ID_M_7','ID_M_8')
    rep <- 'ID_M_'
  } else  if('id_M_1' %in% colnames(troncons)) {
    col_id <- c('gid_new','id_M_1','id_M_2','id_M_3','id_M_4','id_M_5','id_M_6','id_M_7','id_M_8')
    colnames(troncons)[colnames(troncons) == 'gid'] <- 'gid_new'
    rep <- 'id_M_'
  } else {
    stop("could'nt find ID_M_1 or id_M_1 column in shapefile")
  }

  ID_maille <- troncons[,..col_id]
  ID_maille <- as.data.table(lapply(ID_maille,as.numeric))
  ID_maille <- melt(ID_maille,id.vars = 'gid_new',variable.name = 'Colomn',value.name = 'SAFRAN')
  ID_maille$num_Maille <- as.numeric(str_replace(ID_maille$Colomn,rep,''))
  ID_maille <- ID_maille[,Colomn := NULL]
  
  Rap_maille <- troncons[,c('gid_new','Rap_M_1','Rap_M_2','Rap_M_3','Rap_M_4','Rap_M_5','Rap_M_6','Rap_M_7','Rap_M_8')]
  Rap_maille <- as.data.table(lapply(Rap_maille,as.numeric))
  Rap_maille <- melt(Rap_maille,id.vars = 'gid_new',variable.name = 'Colomn',value.name = 'Rapport')
  Rap_maille$num_Maille <- as.numeric(str_replace(Rap_maille$Colomn,'Rap_M_',''))
  Rap_maille <- Rap_maille[,Colomn := NULL]
  
  info_gid_SAFRAN <- merge(ID_maille,Rap_maille, by=c('gid_new','num_Maille'))
  info_gid_SAFRAN <- info_gid_SAFRAN[,num_Maille := NULL]
  info_gid_SAFRAN <- info_gid_SAFRAN[!is.na(SAFRAN)]
  
  return(info_gid_SAFRAN)
}

