

TNETutils_createNetCDF <- function(path_createdNetCDF , variable, units = '',longname = variable,NetCDF_base = NULL, ID_dim = NULL,ID_name = NULL,time_dim = NULL, chunksize = NULL) {
  
  if(is.null(units)){units = ''}
  if (is.null(longname)) {longname = variable}
  if(is.null(ID_name)){ID_name = 'gid'}
  
  if(!is.null(NetCDF_base)){
    base_nc <- nc_open(NetCDF_base)
    
    #get all dims name
    ndims <- base_nc$ndims
    names = c(NA,NA)
    for (i in 1:ndims){
      names[i] <- base_nc$dim[[i]]$name
    }
    
    if (ndims != 2 || !'time' %in% names){
      stop("NetCDF_base must have only 2 dimention with one named 'time'")
    }
    
    
    # get time dimension
    time_var <- ncvar_get(base_nc,'time')
    time_units <- ncatt_get(base_nc,'time')
    # time_netcdf <- nc_read_time(netcdf_file = base_nc)
    
    #get ID dimension
    ID_name = names[names != 'time']
    ID_var <- ncvar_get(base_nc,ID_name)
    ID_arg <- ncatt_get(base_nc,ID_name)
    ID_units <- ID_arg$units
    ID_longname <- ID_arg$long_name
    ID_netcdf <- ncvar_get(base_nc, ID_name)
    
    if (!is.null(ID_dim)) {
      if (length(ID_dim) == length(ID_netcdf)){
        ID_netcdf <- ID_dim
      }else{
        stop(paste('ID_dim must have',length(ID_netcdf),'elements.'))
      }
    }
    nc_close(base_nc)
    
  } else {
    #Get all dimensions info needed
    # ID_name = "gid"
    # ID_units <- list(units =,long_name = ID_name)
    ID_units <-  "Segments_ID"
    ID_longname <- ID_name
    ID_netcdf = ID_dim
    
    
    time_units <- list(units = paste("days since",format(time_dim[1],"%Y-%m-%d")),
                     long_name = "time")
    time_var = 0:(length(time_dim)-1)  
      
  }
  
  if (is.null(chunksize)) {
    chunksize = length(ID_netcdf)
  }else {
    chunksize = min(c(length(ID_netcdf),chunksize))
  }
  
  if (length(units) != length(variable)) {
    units = rep(units,length(variable))
  }
  if (length(longname) != length(variable)) {
    longname = rep(longname,length(variable))
  }
  ##creating dimentions----
  gid_dim <- ncdim_def(name = ID_name,
                       units = ID_units,
                       longname = ID_longname,
                       vals = ID_netcdf)
  
  #get time
  time_dim <- ncdim_def(name = 'time',
                        units = time_units$units,
                        longname = time_units$long_name,
                        vals = time_var)
  
  ##Creating variable
  Nb_col = length(variable)
  list_variable = vector(mode = 'list',length = Nb_col)
  for (i in 1:Nb_col) {
    list_variable[[i]] <- ncvar_def(name = variable[i],
                                    units = units[i],
                                    longname = longname[i],
                                    dim = list(time_dim,gid_dim),
                                    prec = 'double',
                                    chunksizes = c(length(time_var),chunksize))
  }
  
  #Create the file
  if (file.exists(path_createdNetCDF)){file.remove(path_createdNetCDF)}
  createdFile  <-  nc_create(path_createdNetCDF, list_variable)
  
  return(createdFile)
}