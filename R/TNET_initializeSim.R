
#' @title Initialize All info needed by TnetRUI package
#' @description
#' This function will create all working folders and regroup all info needed by TnetRUI [TNET set function](https://tnet-rui-guillaume-hevin-ff27f1d42adcce7ceedbb780ef0d88b292c862.pages.mia.inra.fr/articles/TnetRUI_3.html) in one list.
#' 
#' @param nameSimu *string* Name of the simulation
#' @param yearStart *numeric* Starting year for the simulation
#' @param yearEnd *numeric* Ending year for the simulation
#' @param hydro_model *string* Name of the hydrological model used ('J2000' or 'EROS')
#' @param weather_model *string* Name of the meteological model used ('SAFRAN')
#' @param shapefile *string* Path to the shapefile with rivers to use for TNET.
#' @param path_Export *string* Path where the simulation forlder will be created.
#' @param TNET_path *string* Path to the C++ TNET folder (must finish by "/sources" folder).
#'
#' @return a list of arguments detailed in details section
#' @export
#' 
#' @details
#' **Export Folder organisation**\cr
#' The organisation of the working folder created is the following:\cr
#' \preformatted{
#' path_Export
#'      |______ nameSimu
#'                  |_________ Constant (constantPath)
#'                  |_________ hydro    (hydroPath) 
#'                  |_________ Safran   (safranPath)
#'                  |_________ Shape    (shapePath)
#'                  |_________ tmp      (path_Export_tmp)
#' }
#' This folder organisation is what's expected in order to T-NET to work. More info on [T-NET wiki](https://forgemia.inra.fr/guillaume.hevin/t-net/-/wikis/home)\cr
#' The `tmp` folder will be used for files created but not used by T-NET (such as all files created by [TNET_readJ2K()] or [TNET_readEROS()])
#' \cr
#' \cr     
#' **Argument in returned list**\cr
#' All the arguments in the return list are the following:
#' **Argument name**             | **Created with**          | **TnetRUI function that use it**                                |**info**    
#' ------------------------------|---------------------------|-----------------------------------------------------------------|---------------------------
#' startSimu                     |`yearStart`                |[TNET_readSAFRAN()] [TNET_readJ2K()] [TNET_run()]                | Start of simulation in POSIXct format   
#' endSimu                       |`yearEnd`                  |[TNET_readSAFRAN()] [TNET_readJ2K()] [TNET_run()]                | End of simulation in POSIXct format   
#' hydro_model                   |`hydro_model`              |                                                                 | Path to the folder containing J2K results (ReachLoop.dat and HRULoop.dat)  
#' weather_model                 |`weather_model`            |                                                                 | Path to the folder containing J2K results (ReachLoop.dat and HRULoop.dat)  
#' shapefile                     |`shapefile`                |[TNET_readSAFRAN()]  [TNET_computeQ()]  [TNET_computeHydraulic()]  [TNET_computeQlatSout()]  [TNET_computeTnappe()]   [TNET_modifyVeget()]   [TNET_createShape()]  | Path to shapefile with all river segment info
#' path_Export                   |`path_Export`              | -                                                               | Folder where the simulation is saved.
#' path_Export_tmp               |`path_Export`              |[TNET_readJ2K()]  [TNET_computeQ()]  [TNET_computeQlatSout()]    | Folder where temporary files will be exported.
#' TNET_path                     |`TNET_path`                |[TNET_run()]                                                     | Path to the T-NET C++ code (must finish by '/sources')
#' shapePath                     |`path_Export`              |[TNET_run()]                                                     | Folder where shapefile use by T-NET will be exported. 
#' constantPath                  |`path_Export`              |[TNET_run()]                                                     | Folder where constant files use by T-NET will be exported.  
#' safranPath                    |`path_Export`              |[TNET_run()] [TNET_readSAFRAN()]                                 | Folder where SAFRAN files use by T-NET will be exported. 
#' hydroPath                     |`path_Export`              |[TNET_run()] [TNET_computeQ()] [TNET_computeHydraulic()] [TNET_computeQlatSout()] [TNET_computeTnappe()]   | Folder where Hydraulic and discharge files use by T-NET will be exported. 
#' shapeName                     |`nameSimu`,  `path_Export` |[TNET_run()]                                                     | Name of the shapefile saved in `safranPath`
#' 
#' \cr
#' \cr     
#' **Arguments needed by Hydrological model**\cr
#' \cr
#' *J2000*\cr
#' All arguments to add in the [TNET_initializeSim()] function if the hydrological model used is J2000
#' **Argument name**             | **TnetRUI function that use it**                                |**info**    
#' ------------------------------|-----------------------------------------------------------------|---------------------------
#' `path_J2KResult`              |[TNET_readJ2K()] [TNET_computeQlatSout()]                        | *mandatory* Path to the folder containing J2K results (ReachLoop.dat and HRULoop.dat)  
#' `Bm_method`                   |[TNET_computeHydraulic()]                                        | *mandatory* Method that will be use to estimate river segments width. can be   
#' `path_J2Kparameter`           |[TNET_readJ2K()] [TNET_computeQlatSout()]                        | *optional*  Path to the folder with J2000 parameter files (.par). By default it's `path_J2KResult`.
#' `Largeur_shape`               |[TNET_computeHydraulic()]                                        | *optional*  Path to the shapefile with some rivers mesured width. Needed only when `Bm_method = 'EKrecal'`
#' `quantile.Bm`                 |[TNET_computeHydraulic()]                                        | *optional*  Quantile of discharge where all width in `Largeur_shape` was mesured. Needed only when `Bm_method = 'EKrecal'`
#'
#' \cr
#' \cr     
#' *EROS*\cr
#' All arguments to add in the [TNET_initializeSim()] function if the hydrological model used is EROS
#' 
#' **Argument name**             | **TnetRUI function that use it**                                |**info**    
#' ------------------------------|-----------------------------------------------------------------|---------------------------
#' `path_Qcontributif`           |[TNET_readJ2K()] [TNET_computeQlatSout()]                        | *mandatory* Path to the .txt file containing contributory flow of each EROS subwatershed.
#' `path_Qcum`                   |[TNET_computeHydraulic()]                                        | *mandatory* Path to the .txt file containing totalized flow of each EROS subwatershed.
#' `path_Qnappe`                 |[TNET_readJ2K()] [TNET_computeQlatSout()]                        | *mandatory* Path to the .txt file containing underground flow of each EROS subwatershed.
#' `date_start_data`             |[TNET_computeHydraulic()]                                        | *mandatory - string* Date of the first day of the data in the EROS files. (at the format %Y-%m-%d)
#' \cr
#' \cr     
#' **Arguments needed by the weather model**\cr
#' \cr
#' *SAFRAN*\cr
#' All arguments to add in the [TNET_initializeSim()] function if the weather model used is SAFRAN
#' 
#' **Argument name** | **TnetRUI function that use it** |**info**    
#' ------------------|----------------------------------|-------------------------------------------------------------------
#' `path_SAFRAN`     |[TNET_readSAFRAN()]               | *mandatory* Path to the folder with all hourly SAFRAN NetCDF files.
#'                                                                                                                           
#' 
#' 
#' @examples
#' infoSimu <- TNET_initializeSim(nameSimu = 'Present_2010_2021',
#'                               yearStart = 2010,
#'                               yearEnd = 2021,
#'                               TOPAGE_shape = "/home/ghevin/Documents/TempoVeget/Travail/1_Couplage_TopageJ2000V8/Result/Couplage_J2000V8_Topage.shp",
#'                               path_J2KResult = "/home/ghevin/Documents/Projet_Saone/1_TOPAGE_T-NET/data/Pass_8/J2000/Final_1970_2021_V5",
#'                               path_SAFRAN = '/run/user/9109/gvfs/smb-share:server=ly-data.inra.local,share=ly-unites/Riverly/Hhly/Entrepothh_depot/SAFRAN/NEW/hourly_raw',
#'                               path_Export = "/home/ghevin/Documents/T-NET/Results/Rresults",
#'                               calcul.Bm = 'EKrecalc',
#'                               Largeur_shape = "/home/ghevin/Documents/Projet_Saone/1_TOPAGE_T-NET/data/Pass_8/Largeur_Herve_Pella/Largeur_troncons_hydrographique_decoup_OStrahler.shp",
#'                               quantile.Bm = 0.99,
#'                               TNET_path = '/home/ghevin/Documents/T-NET/Code/TNET/sources',)


TNET_initializeSim <- function(nameSimu,
                               yearStart, 
                               yearEnd,
                               hydro_model = 'J2000',
                               weather_model = 'SAFRAN',
                               shapefile,
                               path_Export,
                               TNET_path,
                               ...) {
                               # TOPAGE_shape,
                               # path_J2KResult,
                               # path_J2Kparameter = path_J2KResult,
                               # path_SAFRAN,
                               # path_Export,
                               # calcul.Bm = 'EK',
                               # Largeur_shape = NULL,
                               # quantile.Bm = NULL,
                               # TNET_path) {
  #
  
  #getting ellipsis arguments----
  ellipsis_arg <- list(...)
  
  
  #Create Start and end of simulation----
  Date_allExport_Start <- as.POSIXct(paste0(yearStart,"-08-01 00:00:00"),format = "%Y-%m-%d %T", tz = "GMT")
  Date_allExport_End <- as.POSIXct(paste0(yearEnd,"-07-31 23:00:00"),format = "%Y-%m-%d %T", tz = "GMT")
  
  
  #create all needed folders----
  Export_folder <- file.path(path_Export,nameSimu)
  Export_folder_shape <- file.path(Export_folder,'Shape')
  Export_folder_cst <- file.path(Export_folder,'Constant')
  Export_folder_safran <- file.path(Export_folder,'Safran')
  Export_folder_hydro <- file.path(Export_folder,'hydro')
  
  path_Export_tmp <- file.path(Export_folder,'tmp')

  if (!dir.exists(Export_folder)) {dir.create(Export_folder,recursive = TRUE)}
  if (!dir.exists(Export_folder_shape)) {dir.create(Export_folder_shape)}
  if (!dir.exists(Export_folder_cst)) {dir.create(Export_folder_cst)}
  if (!dir.exists(Export_folder_safran)) {dir.create(Export_folder_safran)}
  if (!dir.exists(Export_folder_hydro)) {dir.create(Export_folder_hydro)}
  if (!dir.exists(path_Export_tmp)) {dir.create(path_Export_tmp)}
  
  
  
  #Create shapefile name ----
  shapeName <- file.path(Export_folder_shape,paste0('TNET_shape_',nameSimu,'.shp'))
  
  # #TNET Config
  # TnetResults <- file.path(Export_folder,'results',paste0(year(Date_allExport_Start),'-',year(Date_allExport_End)))
  
  #Creating result_list----
  list_needArg <-list(startSimu = Date_allExport_Start,
                      endSimu = Date_allExport_End,
                      hydro_model = hydro_model,
                      weather_model = weather_model,
                      path_Export_tmp = path_Export_tmp,
                      path_Export = Export_folder,
                      shapefile = shapefile,
                      shapePath = Export_folder_shape,
                      constantPath = Export_folder_cst,
                      safranPath = Export_folder_safran,
                      hydroPath = Export_folder_hydro,
                      shapeName = shapeName,
                      TNET_path = TNET_path) #  TNET_result_Path = TnetResults
  
  
  #Getting meteological arguments----
  if (weather_model == 'SAFRAN') {
    needed_arg = c('path_SAFRAN')
    given_arg <- names(ellipsis_arg)
    
    missing_arg <- needed_arg[!needed_arg %in% given_arg]
    if (length(missing_arg) > 0) {
      stop(paste("TNET_initializeSim: Missing arguments when weather_model = 'SAFRAN': ",paste(missing_arg,collapse = ", ")))
    }
    
    list_meteoArg <- ellipsis_arg[needed_arg]
    
    ellipsis_arg <- ellipsis_arg[!given_arg %in% needed_arg]
    
  } else {
    stop('TNET_initializeSim: weather_model must be "SAFRAN"')
  }
  
  list_needArg <- c(list_needArg,list_meteoArg)
  
  
  #Getting needed arguments depending of hydro_model----
  if (hydro_model == 'J2000'){
    
    #J2000 arguments----
    
    ##Verify needed arguments----
    needed_arg <- c('path_J2KResult','Bm_method')
    notMandatori_arg <- c('Largeur_shape', 'Bm_quantile','path_J2Kparameter')
    given_arg <- names(ellipsis_arg)
    
    missing_arg <- needed_arg[!needed_arg %in% given_arg]
    
    # print(!notMandatori_arg %in% given_arg[!given_arg %in% needed_arg])
    
    if (length(missing_arg) > 0) {
      stop(paste("TNET_initializeSim: Missing arguments when hydro_model = 'J2000': ",paste(missing_arg,collapse = ", ")))
    }
    
    
    #Config for Bm calculation
    if (!ellipsis_arg$Bm_method %in% c('EK', 'EKrecal'))
    {
      stop('Bm_method must be "EK" or "EKrecal"')     
    }else if(ellipsis_arg$Bm_method == 'EK')
    {
      if(!is.null(ellipsis_arg$Bm_quantile)) {
        ellipsis_arg$Bm_quantile = NULL
        warning("TNET_initializeSim: Bm_quantile not needed when hydro_model = 'J2000' and Bm_method == 'EK': argument deleted")
      }
      if(!is.null(ellipsis_arg$Largeur_shape)) {
        ellipsis_arg$Largeur_shape = NULL
        warning("TNET_initializeSim: Largeur_shape not needed when hydro_model = 'J2000' and Bm_method == 'EK': argument deleted")
      }
    }else if(ellipsis_arg$Bm_method == 'EKrecal')
    {
      #Check good values for Bm_quantile
      if (is.null(ellipsis_arg$Bm_quantile))
      {
        stop("Bm_quantile provided must be set when 'Bm_method' = 'EKrecal")
      }else if (ellipsis_arg$Bm_quantile<0 || ellipsis_arg$Bm_quantile > 1)
      {
        stop("Bm_quantile must be a number between 0 and 1")
      }
      
      #Check if shapefile_largeur id provided
      if (is.null(ellipsis_arg$Largeur_shape))
      {
        stop("A path to the shapefile with measured width of rivers must be provided in 'Largeur_shape'")
      }
      # 
      # Bm_method = 'EKrecal'
      # Bm_quantile = ellipsis_arg$Bm_quantile
    }
    
    #config for path_J2Kparameter
    if (is.null(ellipsis_arg$path_J2Kparameter)) {
      ellipsis_arg$path_J2Kparameter = ellipsis_arg$path_J2KResult
    }
    
    ##Getting used arguments----
    list_hydroArg <- ellipsis_arg[c(needed_arg,notMandatori_arg)]
    ellipsis_arg <- ellipsis_arg[!given_arg %in% c(needed_arg,notMandatori_arg)]
    
  } else if (hydro_model == 'EROS') {
    
    #EROS arguments----
    
    ##Verify needed arguments----
    needed_arg <- c('path_Qcontributif','path_Qcum','path_Qnappe','path_Tnappe','date_start_data')
    given_arg <- names(ellipsis_arg)
    
    missing_arg <- needed_arg[!needed_arg %in% given_arg]
    
    if (length(missing_arg) > 0) {
      stop(paste("TNET_initializeSim: Missing arguments when hydro_model = 'EROS': ",paste(missing_arg,collapse = ", ")))
    }
    
    
    ##Getting used arguments----
    list_hydroArg <- ellipsis_arg[needed_arg]
    ellipsis_arg <- ellipsis_arg[!given_arg %in% needed_arg]
    
  } else {
    stop('TNET_initializeSim: hydro_model must be "J2000" or "EROS"')
  }
  
  list_needArg <- c(list_needArg,list_hydroArg)
  
  #Warning if not used arguments
  if (length(ellipsis_arg) >0) {
    warning(paste("TNET_initializeSim: Following arguments not used : ",paste(names(ellipsis_arg),collapse = ", ")),immediate. = TRUE)
  }
  # results <- list(startSimu = Date_allExport_Start,
  #                 endSimu = Date_allExport_End,
  #                 path_J2KResult = path_J2KResult,
  #                 path_J2Kparameter = path_J2Kparameter,
  #                 path_Export_tmp = path_Export_tmp,
  #                 path_Export = Export_folder,
  #                 TOPAGE_shape = TOPAGE_shape,
  #                 Largeur_shape = Largeur_shape,
  #                 Bm_method = Bm_method,
  #                 Bm_quantile = Bm_quantile,
  #                 shapePath = Export_folder_shape,
  #                 constantPath = Export_folder_cst,
  #                 safranPath = Export_folder_safran,
  #                 path_SAFRAN = path_SAFRAN,
  #                 hydroPath = Export_folder_hydro,
  #                 shapeName = shapeName,
  #                 TNET_path = TNET_path,
  #                 TNET_result_Path = TnetResults)
  
  # results = ellipsis_arg
  
  #Saving results in ExportFolder
  # qsave(results, file = file.path(Export_folder, paste0('info_simu',nameSimu,'.qs')))
  return(list_needArg)
}


# 
# TNET_setExutoire <- function(troncon, ID_exutoire) {
#   gid_ok <- troncon[gid_new == ID_exutoire,]
#   tronconsBV <- c(gid_ok$gid_new)
#   gid_before <- troncon[ID_ND_FIN == gid_ok$ID_ND_INI]
#   while(nrow(gid_before) > 0){
#     tronconsBV <- c(tronconsBV,gid_before$gid_new)
#     gid_before <- troncon[ID_ND_FIN %in% gid_before$ID_ND_INI]
#   }
#   
#   return(tronconsBV)
# }




