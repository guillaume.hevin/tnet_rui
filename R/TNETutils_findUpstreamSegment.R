


#' Find all segments ID upstream 
#' 
#' @description
#' Will find every ID of all segments upstream of the segment selected
#' 
#' @usage TNETutils_findUpstreamSegment(segments, ID)
#' @param segments *data.frame* All segments in the shapefile. must have the following columns:
#' |             |                                                                                                                                  |
#' |-------------|----------------------------------------------------------------------------------------------------------------------------------|
#' | `gid_new`   | ID of the Topage segment                                                                                                         |
#' | `ID_ND_INI` |  ID of the initial node of the segment.                                                                                          |
#' | `ID_ND_FIN` |  ID of the final node of the segment.                                                                                            |
#' 
#' @param ID *numeric* ID of the segment to start the search
#'
#' @return a vector with all segments ID found
#' @export
#' @seealso [TNETshape_removePbDrainArea()]
#' @examples
#' # Read exemple shapefile
#' shape_name <- system.file("extdata","TestNetwork_Ardiere.shp", package = "TnetRUI")
#' shape_data <- st_read(shape_name)
#' 
#' # found all IDs upstream of the segment 1671262
#' ID_found <- TNETutils_findUpstreamSegment(segments = shape_data, ID=1671262)
#' ID_found
TNETutils_findUpstreamSegment <- function(segments, ID){

  segments <- segments[,c("gid_new","ID_ND_INI","ID_ND_FIN")]
  ID_search <- segments$ID_ND_INI[segments$gid_new == ID]
  
  gid_found <- c()
  while(any(ID_search %in% segments$ID_ND_FIN)){
    found <- segments[segments$ID_ND_FIN %in% ID_search,]
    gid_found <- c(gid_found,found$gid_new)
    ID_search <- found$ID_ND_INI
  }

  return(gid_found)
}