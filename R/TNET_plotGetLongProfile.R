
TNET_plotGetLongProfile <- function(Nom_riviere,troncons,column.keep = NULL, confluence.toAdd = NULL,export.folder = NULL,var.plot =NULL,var.name=NULL){
  
  #Columns we need for calculation
  column = c("gid","TopoOH","L_km","ND_INI","ND_FIN")
  
  #we keep columns ask by user
  if(!is.null(column.keep)){
    column <- c(column,column.keep)
  }
  
  #we keep column to plot, ask by user
  if(!is.null(var.plot) && !(var.plot %in% column)) {
    column <- c(column,var.plot)
  }
  
  #Select need/ask columns
  troncons_L <- subset(troncons,
                       select = column)
  
  #Small calculation on segments data.table
  troncons_L <- setDT(troncons_L)
  troncons_L$TopoOH[is.na(troncons_L$TopoOH)] <- 'NA'
  
  #Get all rivers to research
  all_Riv = unique(c(Nom_riviere,confluence.toAdd))
  
  Nb_Riviere = length(Nom_riviere) #Number of Profile to export
  Nb_confluence = length(confluence.toAdd) #Number of confluence to add
  Nb_all <- length(all_Riv) #Number of all rivers 
  
  #initialize list that will receive all found data
  all_gid_list <- vector(mode='list', length=Nb_Riviere)
  all_gid_exutoire_list <- vector(mode='list', length=Nb_confluence)

  #We are searching all segments on all rivers (even confluences), in order to keep 
  #the longer one to be sure to have the good riche
  for (r in 1:Nb_all){
    
    #Get river name
    riviere_study <- all_Riv[r]
    gid_Riviere <- troncons_L[str_detect(TopoOH,riviere_study),]
    
    if (nrow(gid_Riviere)>0){
      gid_start <- troncons_L[(str_detect(TopoOH,riviere_study)),]
      gid_start <- gid_start[(!ND_INI %in% ND_FIN),]
      
      
      Nb_start_found <- nrow(gid_start)
      gid_start$L_tot <- gid_start$L_km
      #Si on trouve plusieurs sources possible, on va toutes les étudier et prendre la plus grande
      
      L_tot_max_found <- 0
      for (i in 1:Nb_start_found){
        gid_start_study <- gid_start[i,]
        results_study <- vector("list", length(gid_Riviere))
        t <- 1
        
        gid_start_study$num <- t
        
        results_study[[t]] <- gid_start_study
        L_tot <-  gid_start_study$L_tot
        new_ID_INI <- gid_start_study$ND_FIN
        
        
        while(1){
          gid_suiv <- troncons_L[ND_INI == new_ID_INI,]
          if (nrow(gid_suiv)==1 && (str_detect(gid_suiv$TopoOH,riviere_study) || gid_suiv$TopoOH == 'NA')){ #
            
            L_tot <-  L_tot + gid_suiv$L_km
            gid_suiv$L_tot <- L_tot
            new_ID_INI <- gid_suiv$ND_FIN
            
            t <- t+1
            gid_suiv$num <- t
            results_study[[t]] <- gid_suiv
          }else{
            break
          }
        }
        
        Riv_all_study <- rbindlist(results_study)
        
        Nom_riviere_txt <- unlist(strsplit(riviere_study,split = '|',fixed = TRUE))[1]
        
        Riv_all_study$Nom <- Nom_riviere_txt
        
        if (L_tot_max_found < L_tot){
          Riv_all <- Riv_all_study
          L_tot_max_found <- L_tot
        }
      }
      
      if (riviere_study %in% Nom_riviere) {
        ind = which(riviere_study == Nom_riviere)
        all_gid_list[[ind]] <- Riv_all
      }
      
      if (riviere_study %in% confluence.toAdd) {
        ind = which(riviere_study == confluence.toAdd)
        all_gid_exutoire_list[[ind]] <- Riv_all[L_tot == max(L_tot),c("ND_FIN","Nom")]
      }
      
    }
    
    
  }
  
  all_exutoire <- rbindlist(all_gid_exutoire_list)
  
  #Put confluence in file
  for (r in 1:Nb_Riviere){
    data <- all_gid_list[[r]]
    riv = unique(data$Nom) 
    
    conflu = all_exutoire[ND_FIN %in% data$ND_FIN & (Nom != riv),]
    colnames(conflu)[colnames(conflu) == "Nom"] <- "Confluence"
    
    data <- merge(data,conflu,all.x=TRUE)
    all_gid_list[[r]] <- data[order(num)]
  }
  
  
  if (!is.null(var.plot)){
    if (is.null(export.folder)) {
      export.folder = getwd()
    } else if (!dir.exists(export.folder)) {
      dir.create(export.folder,recursive = TRUE)
    }
    if (is.null(var.name)) {
      var.name = ""
    }
    
    for (i in 1:Nb_Riviere){
      data <- all_gid_list[[i]]
      riv = unique(data$Nom) 
      
      colnames(data)[colnames(data) == var.plot] <- "varPLOT"
      plt <- ggplot(data = data, aes(x=L_tot))+
        geom_vline(xintercept = data$L_tot[!is.na(data$Confluence)], color = 'grey')+
        annotate(geom="label", x=data$L_tot[!is.na(data$Confluence)], y=min(data$varPLOT), label=data$Confluence[!is.na(data$Confluence)], angle=90,hjust = 0,fill='white',size = 3)+
        geom_line(aes(y=varPLOT),color = '#275662')+ #,colour = OSTRAHLER
        scale_y_continuous(name = var.name)+
        scale_x_continuous(breaks = floor(seq(min(data$L_tot), max(data$L_tot), by = 25)))+
        theme_inrae()+
        xlab("Distance to source (km)")+
        ggtitle(sprintf('%s river',riv))+
        theme(plot.title = element_text(hjust = 0.5))
      # ggplotly(plt)
      # plt
      ggsave(filename = file.path(export.folder,paste0(var.plot,'_',riv,'_longRiver.png')), plot = plt,width = 300, height = 150, units = "mm") 
      
    }
   
    cat(paste(Nb_Riviere,'figure saved !'))
  }
 
  return(rbindlist(all_gid_list))
}
