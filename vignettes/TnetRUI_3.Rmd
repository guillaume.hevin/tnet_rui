---
title: "3. Other functions"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{3. Other functions}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

All these functions are here to facilitate the use of repetitive task used in the package.

# TNETutils functions
- **`TNETutils_findUpstreamSegment()`** will get the IDs of all segments upstream of a given ID.

- **`TNETutils_getDatInfoJ2K()`** will read the first line of the "HRULoop.dat" or "Reachloop.dat" file and extract the following informations:

  - The line where the data begin in the file.

  - The first and last day of the simulation (and the number of them).

  - All column names in the file and the number of them.
  
- **`TNETutils_getSAFRAN_fromShape()`** will read all SAFRAN related columns in the shapefile and put them in a data.frame with 3 columns: `gid_new`, `SAFRAN` and `Rapport`.

- **`TNETutils_getShape_fromNETCDF()`** will read the attribute relative to the shapefile in the NetCDF and open it.

# TNETdiagnostic functions
This set of function will verify if there is any problems with computation made by other functions.

- **`TNETdiagnostic_pbDrainArea()`** will catch any problems with drain area computations, and display a warning message to the user.

  - If some segments have very little drain area compared to the downstream segment drain area (with a threshold limit by default set to 40%).
  
  - If some segments have downstream drain area smaller than upstreams ones.
  
  - If some segments have drain area not calculated (NULL or NaN).

