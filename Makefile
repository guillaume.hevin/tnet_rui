all: doc
.PHONY: doc install check

doc:
	R -e 'devtools::document()'

website:
	R -e 'devtools::document()'
	R -e 'usethis::use_pkgdown() ; pkgdown::build_site()'

install:
	R -e "remotes::install_gitlab(repo = 'guillaume.hevin/tnet_rui', host = 'forgemia.inra.fr')"

check:
	R -e 'devtools::check()'

